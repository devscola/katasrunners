class StringCalculator {
    calculate(string) {
        if (this._isEmpty(string)) return 0;
        const match = string.replace(/\n|;/g, ',')
        const numbers = match.split(',', 50);
        const final = this._isNegativeAndBigNumber(numbers)

        return final;
    }
    _isEmpty(string) {
        return string === '';
    }

    _isNegativeAndBigNumber(string) {
        let msg = '';
        let result = '';
        let flag = 1;
        for (let i of string) {
            if (parseInt(i) < 0) {
                flag = 0;
                msg += ' ' + parseInt(i)
            }
        }

        let msgneg = "negatives not allowed:" + msg

        result = 0;
        for (let i of string) {
            if (parseInt(i) <= 1000) {
                result += parseInt(i);
            }
        }
        if (flag == 0) { return msgneg } else { return result }
    }

}
module.exports=StringCalculator