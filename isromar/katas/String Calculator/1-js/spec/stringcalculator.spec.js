import { stringCalculator } from '../src/stringcalculator.js'

describe('stringCalculator', () => {
/*
    it('should return error message if string contains more than 2 numbers', () => {
        // Arrange
        const expected_value = 'Solo admite dos números'

        // Act
        const result = stringCalculator('258')

        // Assert
        expect(result).toBe(expected_value)
    })
*/
    it('should return 0 if string is empty', () => {
        // Arrange
        const expected_value = 0

        // Act
        const result = stringCalculator('')

        // Assert
        expect(result).toBe(expected_value)

    })

    it('should return the same number if string has only one number', () => {
        // Arrange
        const expected_value = 5

        // Act
        const result = stringCalculator('5')

        // Assert
        expect(result).toBe(expected_value)

    })

    it('should return the sum of the string', () => {
        // Arrange
        const expected_value = 14   // 6 + 8

        // Act
        const result = stringCalculator('6,8')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return the sum of the string', () => {
        // Arrange
        const expected_value = 25   // 5 + 8 + 2 + 3 +7

        // Act
        const result = stringCalculator('5,8,2,3,7')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should handle new lines between numbers ', () => {
        // Arrange
        const expected_value = 45   // 31 + 3 + 8 + 2 + 1

        // Act
        const result = stringCalculator('31\n3,8,2\n,1')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('shouldn\'t handle new lines without numbers next to \n', () => {
        // Arrange
        const expected_value = "No admite '\n' sin un número que lo acompañe"

        // Act
        const result = stringCalculator('6,\n,8,2\n,1')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should handle new separators', () => {
        // Arrange
        const expected_value = 1151  // 104 + 36 + 1000+ 8 + 2 + 1

        // Act
        const result = stringCalculator('//;\n104\n36;1000;8;2\n;1')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('shouldn\'t handle negative numbers', () => {
        // Arrange
        const expected_value = "negatives not allowed"

        // Act
        const result = stringCalculator('\n-5\n3;8;2\n;-10')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should change numbers bigger than 1000', () => {
        // Arrange
        const expected_value = 894  // 91 + 3 + 800;

        // Act
        const result = stringCalculator('91,\n3,800,1023,2000')

        // Assert
        expect(result).toBe(expected_value)
    })
})
