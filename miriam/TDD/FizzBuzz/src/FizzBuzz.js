export class FizzBuzz {
   
    comprobador(number) {

        const fizz = "Fizz"
        const buzz = "Buzz" 
        const fizzBuzz = fizz + buzz
        const numberStr = number.toString()


        if (this.isFizzBuzz(number)){
            return fizzBuzz
        }
        if (this.isFizz(number) || numberStr.includes(3)) {
            return fizz
        }
        if (this.isBuzz(number) || numberStr.includes(5)) {
            return buzz
         }
        return numberStr
        
    }


    isFizz(number){
        return number%3 === 0
    }
    
    isBuzz(number){
        if(number%5 === 0){
            return true
        } else {
            return false
        }
    }

    isFizzBuzz(number){
        return this.isFizz(number) && this.isBuzz(number)
    }

   

}