import { devuelvemayordedoscartas, devuelveManoMasAlta, compararPares, compararDoblePar, mayorDeTres, escalera, trioYPar, poker} from '../src/pokerhands'

describe('valor más alto', () => {
    it('should return true', () => {
        // Arrange
        const expected_value = 'J'

        // Act
        const result = devuelvemayordedoscartas('J', '5')

        // Assert
        expect(result).toBe(expected_value)
    })
})

describe('Mano más alta', () => {
    it('should return true', () => {
        // Arrange
        const mano1 = ['2','4','5','6','J']
        const mano2 = ['2','3','4','6','K']
        const expected_value = mano2

        // Act
        const result = devuelveManoMasAlta(mano1,mano2)

        // Assert
        expect(result).toBe(expected_value)
    })
})



describe('Gana Par más alto', () => {
    it('should return true', () => {
        // Arrange
        const mano1 = ['2','2','4','6','J']
        const mano2 = ['3','3','4','6','J']
        const expected_value = mano2

        // Act
        const result = compararPares(mano1,mano2)

        // Assert
        expect(result).toBe(expected_value)
    })
})

describe('Gana dobles parejas más altas', () => {
    it('should return true', () => {
        // Arrange
        const mano1 = ['2','2','4','4','J']
        const mano2 = ['3','3','J','6','J']

        const expected_value = ['3','3','J','6','J']

        // Act
        const result = compararDoblePar(mano1,mano2)

        // Assert
        expect(result).toEqual(expected_value)
    })
})

describe('Gana trío más alto', () => {
    it('should return true', () => {
        // Arrange
        const mano1 = ['2','2','2','4','J']
        const mano2 = ['3','3','3','6','J']

        const expected_value = ['3','3','3','6','J']

        // Act
        const result = mayorDeTres(mano1,mano2)

        // Assert
        expect(result).toEqual(expected_value)
    })
})

describe('Gana escalera más alta', () => {
    it('should return true', () => {
        // Arrange
        const mano1 = ['2','3','4','5','6']
        const mano2 = ['3','4','5','6','7']

        const expected_value = ['3','4','5','6','7']

        // Act
        const result = escalera(mano1,mano2)

        // Assert
        expect(result).toEqual(expected_value)
    })
})

describe('Gana trio y par', () => {
    it('should return true', () => {
        // Arrange
        const mano1 = ['2','2','2','3','3']
        const mano2 = ['3','3','3','2','2']

        const expected_value = ['3','3','3','2','2']

        // Act
        const result = trioYPar(mano1,mano2)

        // Assert
        expect(result).toEqual(expected_value)
    })
})

describe('poker', () => {
    it('should return true', () => {
        // Arrange
        const mano1 = ['2','2','2','2','J']
        const mano2 = ['3','3','3','3','J']

        const expected_value = ['3','3','3','3','J']

        // Act
        const result = poker(mano1,mano2)

        // Assert
        expect(result).toEqual(expected_value)
    })
})

