import { fizzbuzz } from '../src/fizzbuzz.js'

describe('Fizzbuzz', () => {

  it('Si el número es divisible por 3, escribe "Fizz" en su lugar.', () => {

    // Arrange
    const expected_value = 'Fizz';
    const not_expected_value = 'Buzz';

    // Act
    const result = fizzbuzz(3);

    // Assert
    expect(result).toEqual(expected_value);
    expect(result).not.toEqual(not_expected_value);
  })

  it('Si el número es divisible por 5, escribe "Buzz" en su lugar.', () => {

    // Arrange
    const expected_value = 'Buzz';
    const not_expected_value = 'Fizz';

    // Act
    const result = fizzbuzz(5);

    // Assert
    expect(result).toEqual(expected_value);
    expect(result).not.toEqual(not_expected_value);
  })

  it('Si el número es divisible por 3 y por 5, escribe "FizzBuzz" en su lugar.', () => {

    // Arrange
    const expected_value = 'FizzBuzz';
    const not_expected_value = 'Fizz';

    // Act
    const result = fizzbuzz(15);

    // Assert
    expect(result).toEqual(expected_value);
    expect(result).not.toEqual(not_expected_value);
  })

  it('Si el número no es divisible por 3 ni por 5, convierte el número en String.', () => {

    // Arrange
    const expected_value = '7';
    const not_expected_value = 7;

    // Act
    const result = fizzbuzz(7);

    // Assert
    expect(result).toEqual(expected_value);
    expect(result).not.toEqual(not_expected_value);
  })

  it('Comprobación de los resultados al dar un rango de números', () => {

    // Arrange
    const expected_value = ['1','2','Fizz','4','Buzz','Fizz','7','8','Fizz','Buzz','11','Fizz','Fizz','14','FizzBuzz','16','17','Fizz'];

    // Act
    const array_de_valores = [];
    for (var valor_del_item = 1 ; valor_del_item <= 18; valor_del_item++){
      array_de_valores.push(valor_del_item);
    }

    // Assert
    const array_de_valores_modificado = array_de_valores.map(fizzbuzz);
    expect(array_de_valores_modificado).toEqual(expected_value);
  })

  it('A number is fizz if it is divisible by 3', () => {

    // Arrange
    const expected_value = 'Fizz';

    // Act
    const result = fizzbuzz(9);
    const not_result = fizzbuzz(8);

    // Assert
    expect(result).toEqual(expected_value);
    expect(not_result).not.toMatch(expected_value);
  })

  it('A number is fizz if it has a 3 in it.', () => {

    // Arrange
    const expected_value = 'Fizz';

    // Act
    const result = fizzbuzz(430);
    const not_result = fizzbuzz(182);

    // Assert
    expect(result).toMatch(expected_value);
    expect(not_result).not.toMatch(expected_value);
  })

  it('A number is buzz if it is divisible by 5', () => {

    // Arrange
    const expected_value = 'Buzz';

    // Act
    const result = fizzbuzz(25);
    const not_result = fizzbuzz(8);

    // Assert
    expect(result).toEqual(expected_value);
    expect(not_result).not.toMatch(expected_value);
  })

  it('A number is buzz if it has a 5 in it.', () => {

    // Arrange
    const expected_value = 'Buzz';

    // Act
    const result = fizzbuzz(425);
    const not_result = fizzbuzz(182);

    // Assert
    expect(result).toMatch(expected_value);
    expect(not_result).not.toMatch(expected_value);
  })

  it('A number is FizzBuzz if it is divisible by 3 and 5', () => {

    // Arrange
    const expected_value = 'FizzBuzz';

    // Act
    const result = fizzbuzz(45);
    const not_result = fizzbuzz(16);

    // Assert
    expect(result).toMatch(expected_value);
    expect(not_result).not.toMatch(expected_value);
  })

  it('A number is FizzBuzz if f it has a 3 or a 5 in it.', () => {

    // Arrange
    const expected_value = 'FizzBuzz';

    // Act
    const result = fizzbuzz(352);
    const not_result = fizzbuzz(17);

    // Assert
    expect(result).toMatch(expected_value);
    expect(not_result).not.toMatch(expected_value);
  })

})