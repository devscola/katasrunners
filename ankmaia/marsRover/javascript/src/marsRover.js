const NORTH = 0
const EAST = 1
const SOUTH = 2
const WEST = 3
const DIMENSION = 5


class MoveRule {

    static execute(x,y,direction){
        switch (direction) {
            case NORTH:
                y = ++y % (DIMENSION + 1)
                break
            case EAST:
                x = ++x % (DIMENSION + 1)
                break
            case SOUTH:
                y--
                if (y < 0) {
                    y = DIMENSION
                }

                break
            case WEST:
                x--
                if (x < 0) {
                    x = DIMENSION
                }
                break
        }
        return [x,y,direction]
    }
}
class RotateLeftRule {
    static execute(x,y,direction){
        direction--
        if (direction < 0) {
            direction = 3
        }
        return [x,y,direction]
    }

}
class RotateRightRule  {
    static execute(x,y,direction){
        direction = ++direction % 4
        return [x,y,direction]
    }

}
export class MarsRover {

    constructor(positionX, positionY, direction, commands = "") {

        this.positionX = positionX
        this.positionY = positionY
        this.direction = direction
        this.commands = commands
        this.executeCommands()
    }
    
    executeCommands() { 
        const rules = {
            "L" : RotateLeftRule,
            "R" : RotateRightRule,
            "M" : MoveRule
        }

        for(const command of this.commands){
            const parameter = rules[command].execute(this.positionX,this.positionY,this.direction)
            this.positionX = parameter[0]
            this.positionY = parameter[1]
            this.direction = parameter[2]
        }
        
    }
    
    getPosition() {
        return [this.positionX, this.positionY, this.direction]  
    }

    getCommands() {
        return this.commands
    }
}