const FizzBuzz=require('../src/fizzbuzz.js')

describe('Fizzbuzz', () => {
    it('Returns Fizz if a divisible by 3 number is given', () => {
        // Arrange (preparación)
        const divisibleBy3 = 9
        const fizzBuzz = new FizzBuzz()

        // Act (poner en prueba nuestra app)
        const result = fizzBuzz.calculate(divisibleBy3)

        // Assert (comprobar que funciona)
        expect(result).toBe('Fizz')
    })

    it('Returns Buzz if a divisible by 5 number is given', () => {
        const fizzBuzz = new FizzBuzz()
        const divisibleBy5 = 10
       
        const result = fizzBuzz.calculate(divisibleBy5)

        expect(result).toBe('Buzz')

    })


    it('Returns Fizzbuzz if the given number is divisibe by both 3 and 5', () => {
        const fizzBuzz = new FizzBuzz()
        const divisibleBy3and5 = 45

        const result = fizzBuzz.calculate(divisibleBy3and5)

        expect(result).toBe('FizzBuzz')
    })

    it(' Writes the string representation of the number if the number is not divisible neither by 3 nor 5', () => {
        //Arrange
        const fizzBuzz = new FizzBuzz()
        const notDivisibleBy3and5 = 8

        //Act
        const result = fizzBuzz.calculate(notDivisibleBy3and5)

        //assert
        expect(result).toBe('8')
    })

})

