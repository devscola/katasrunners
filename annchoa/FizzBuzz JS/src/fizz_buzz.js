export class FizzBuzz {


  get_string(number){
    if (this.is_fizz_buzz(number)){
      return "FizzBuzz"
    }
    if (this.is_fizz(number)){
      return "Fizz"
    }
    if (this.is_buzz(number)){
      return "Buzz";
    }
    if (!this.is_fizz_buzz(number)){
      return number.toString()
    }
//    if (!(isFizzBuzz())){
//      return str(number)
//    }

}

  is_fizz_buzz(number){
      return this.is_fizz(number) && this.is_buzz(number)
   }

  is_fizz(number){
    const string_number = number.toString()
    return number % 3 === 0 || string_number.includes("3")
  }

  is_buzz(number){
    const string_number = number.toString()
    return number % 5 == 0 || string_number.includes("5")
  }
}
