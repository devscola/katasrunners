module.exports = function FizzBuzz() {
    let fizzArray = new Array(100).fill('');
    for (let i = 0; i < 100; i++) {
        if (isFizz(i + 1)) {
            fizzArray[i] += 'Fizz';
        } 
        if (isBuzz(i + 1)) {
            fizzArray[i] += 'Buzz';
        } 
        if (!fizzArray[i]) {
            fizzArray[i] += i + 1;
        }
    }
    return fizzArray;
}

function isFizz(num) {
    return isMultipleOf(3, num) || contains('3', num);
}

function isBuzz(num) {
    return isMultipleOf(5, num) || contains('5', num);
}

function isMultipleOf(multiplier, num) {
    return ((num) % multiplier) === 0;
}

function contains(digit, num) {
    const splitNumber = num.toString().split('');
    return (splitNumber.indexOf(digit) >= 0);
}



function isMultipleOf5(num) {
    return ((num) % 5) === 0;
}

function contains5(num) {
    const splitNumber = num.toString().split('');
    return (splitNumber.indexOf('5') >= 0);
}
