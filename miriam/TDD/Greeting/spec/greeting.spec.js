import { Greeting } from '../src/greeting.js'

describe('Example', () => {
    it('should return "Hello, Bob" when the name is Bob', () => {
        // Arrange
        const greetingClass = new Greeting()
        const nombre = "Bob"
        
        // Act
        const result = greetingClass.greet(nombre)

        // Assert
        expect(result).toBe("Hello, Bob")
    })

    it('should return "Hello, my friend" when name is null', () => {
        //Arrange
        const greetingClass = new Greeting()
        //const name = null ----> es bien o no declarar esta variable?
        const noName = "my friend"

        //Act
        const result = greetingClass.greet(noName)

        //Assert
        expect(result).toBe("Hello, my friend")

    })

    it('should return "HELLO JERRY!" when name is all uppercase', () => {
        //Arrange
        const greetingClass = new Greeting()
        const uppName = "JERRY"

        //Act
        const result = greetingClass.greet(uppName)

        //Assert
        expect(result).toBe("HELLO JERRY!")
    })

    it('should return "Hello, Jill and Jane" when name is an array of two names', () => {
        //Arrange
        const greetingClass = new Greeting()
        const nameArr = ["Jill and Jane"]

        //Act
        const result = greetingClass.greet(nameArr)

        //Assert
        expect(result).toBe("Hello, Jill and Jane")
    })

    it('should return "Hello Amy, Brian, and Charlotte" when there are more than two names', () => {
        //Arrange
        const greetingClass = new Greeting()
        const name3Arr = ["Amy, Brian, and Charlotte"]

        //Act
        const result = greetingClass.greet(name3Arr)

        //Assert
        expect(result).toBe("Hello, Amy, Brian, and Charlotte")
    })

    it('should return "Hello, Amy and Charlotte. AND HELLO BRIAN! when mix of normal and shouted names by separating the response into two greetings', () => {
        //Arrange
        const greetingClass = new Greeting()
        const greetFrst = ["Amy and Charlotte"]
        const greetScnd = ["BRIAN"]

        //Act
        const result = greetingClass.greet(greetFrst) + ". AND " + greetingClass.greet(greetScnd)

        //Assert
        expect(result).toBe("Hello, Amy and Charlotte. AND HELLO BRIAN!")
    })
})
