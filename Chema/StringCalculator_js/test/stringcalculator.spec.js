const StringCalculator=require('../src/stringCalculator.js')

describe('stringCalculator', () => {

    it('return 0 if an empty string is given', () => {
        //Arrange(preparación)
        const emptyString = '';
        const stringCalculator = new StringCalculator;
        //Act (poner a prueba nuestra App)
        let sum = stringCalculator.calculate(emptyString);
        //Assert (comprobar que funciona)
        expect(sum).toBe(0);
    });

    it('return the number if only one number is given', () => {
        //Arrange(preparación)
        const oneNumberString = '1';
        const stringCalculator = new StringCalculator;
        //Act (poner a prueba nuestra App)
        let sum = stringCalculator.calculate(oneNumberString);
        //Assert (comprobar que funciona)
        expect(sum).toBe(1);
    })

    it('return the sum of both numbers if two numbers are given', () => {
        //Arrange(preparación)
        const twoNumbersString = '1,4';
        const stringCalculator = new StringCalculator;
        //Act (poner a prueba nuestra App)
        let sum = stringCalculator.calculate(twoNumbersString);
        //Assert (comprobar que funciona)
        expect(sum).toBe(5);
    })

    it('returns the sum of any number of numbers', () => {
        //Arrange(preparación)
        const anyNumbersString = '8,2,5';
        const stringCalculator = new StringCalculator;
        //Act (poner a prueba nuestra App)
        let sum = stringCalculator.calculate(anyNumbersString);
        //Assert (comprobar que funciona)
        expect(sum).toBe(15);
    })

    it('returns the sum if the numbers have new lines', () => {
        //Arrange(preparación)
        const anyNumbersAndNewLinesString = '1 \n 2,3, \n';
        const stringCalculator = new StringCalculator;
        //Act (poner a prueba nuestra App)
        let sum = stringCalculator.calculate(anyNumbersAndNewLinesString);
        //Assert (comprobar que funciona)
        expect(sum).toBe(6);
    })

    it('can use any delimiter for the string', () => {
        //Arrange(preparación)
        const anyNumbersAndDelimiterString = '5;2;3';
        const stringCalculator = new StringCalculator;
        //Act (poner a prueba nuestra App)
        let sum = stringCalculator.calculate(anyNumbersAndDelimiterString);
        //Assert (comprobar que funciona)
        expect(sum).toBe(10);
    })

    it('should not allow negative numbers', () => {
        //Arrange(preparación)
        const negativeNumbersString = '5,-4';
        const stringCalculator = new StringCalculator;
        //Act (poner a prueba nuestra App)
        let sum = stringCalculator.calculate(negativeNumbersString);
        //Assert (comprobar que funciona)
        expect(sum).toBe('negatives not allowed: -4');
    })

    it('returns only the sum of the numbers less than 1000', () => {
        //Arrange(preparación)
        const bigNumbersString = '5,1004,3';
        const stringCalculator = new StringCalculator;
        //Act (poner a prueba nuestra App)
        let sum = stringCalculator.calculate(bigNumbersString);
        //Assert (comprobar que funciona)
        expect(sum).toBe(8);
    })



})