import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MarsRoverTest {
	
	@Test
	void test1() {//'should know its landing position'
		MarsRover rover = new MarsRover(5,5,'N');
		String obtenido=MarsRover.move(rover,"");
		String esperado="MarsRover [5, 5, N]";
		assertEquals(esperado,obtenido);
	}
	
	@Test
	void test2() {//'should know its final position'
		MarsRover rover = new MarsRover(1,2,'N');
		String obtenido=MarsRover.move(rover,"MMM");
		String esperado="MarsRover [1, 5, N]";
		assertEquals(esperado,obtenido);
	}
	
	@Test
	void test3() {//''should move foward when receive the command M''
		MarsRover rover = new MarsRover(3,2,'N');
		String obtenido=MarsRover.move(rover,"MM");
		String esperado="MarsRover [3, 4, N]";
		assertEquals(esperado,obtenido);
	}
	
	@Test
	void test4() {//'should turn 90 degrees right when recives command R'
		MarsRover rover = new MarsRover(2,2,'N');
		String obtenido=MarsRover.move(rover,"RM");
		String esperado="MarsRover [3, 3, E]";
		assertEquals(esperado,obtenido);
	}
}
