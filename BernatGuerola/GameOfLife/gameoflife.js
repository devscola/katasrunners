export class gof {
    constructor(){
        this.contadorvecinos=0
        this.mort="."
        this.viu ="*"
    }

    rellenarmatrizrandom(size){
        let matriz = []
        for (let x = 0; x < size; x++){
            matriz[x]=[]
            for (let y = 0; y < size; y++){
                let result = Math.floor(Math.random() * (2 - 0)) + 0
                if (result == 1){
                    matriz[x][y] = this.viu;
                } else if (result == 0) {
                    matriz[x][y] = this.mort;
                }             
              }
        }
        return matriz  
    }

    imprimirMatriz(matriz){
        let tamany = matriz.length
        for (let x = 0; x < tamany; x++){
            let row=""
            for (let y = 0; y < tamany; y++){
                row+=matriz[x][y]+"    "         
            }
            console.log(row)
        }
    }

    contadorvecinos(matriz, pos1, pos2){
        if (matriz [pos1-1][pos2-1]==viu){
            this.contadorvecinos+=1
        }
        if (matriz [pos1-1][pos2]==viu){
            this.contadorvecinos+=1
        }
        if (matriz [pos1-1][pos2+1]==viu){
            this.contadorvecinos+=1
        }
        if (matriz [pos1][pos2-1]==viu){
            this.contadorvecinos+=1
        }
        if (matriz [pos1][pos2+1]==viu){
            this.contadorvecinos+=1
        }
        if (matriz [pos1+1][pos2-1]==viu){
            this.contadorvecinos+=1
        }
        if (matriz [pos1+1][pos2]==viu){
            this.contadorvecinos+=1
        }
        if (matriz [pos1+1][pos2+1]==viu){
            this.contadorvecinos+=1
        }
    }

    resuelveVida(inicial, contadorvecinos){
        let result = 'hola'
        if (inicial==this.viu && (contadorvecinos==0 || contadorvecinos==1)){result= this.mort}
        if (inicial==this.viu && (contadorvecinos==4 || contadorvecinos==5 || contadorvecinos==6 || contadorvecinos==7 || contadorvecinos==8)){result= this.mort}
        if (inicial==this.viu && (contadorvecinos==2 || contadorvecinos==3)){result= this.viu}
        if (inicial==this.mort && contadorvecinos>=3) {result= this.viu}
        return result
    }

    nextgeneration(matriz){
        for (let x = 0; x < matriz.size; x++){
            for (let y = 0; y < matriz[x].size; y++){
                let inicial = matriz[x][y]
                matriz[x][y]=this.resuelveVida(inicial, this.contadorvecinos(matriz, x,y))
                }         
            }
        return matriz
    }

    joc(size){
        let matriz1=this.rellenarmatrizrandom(size)
        console.log("\nPrimera generación\n")
        this.imprimirMatriz(matriz1)
        console.log("\nSegunda generación\n")
        let matriz2=this.nextgeneration(matriz1)
        this.imprimirMatriz(matriz2)
    }
}
