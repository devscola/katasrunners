export function mars_rover(landingLoc, commands, planetSize) {
    if(commands===undefined) return landingLoc;
    else {
        const cardinalDirections = ['N','E','S','W']
        if(planetSize===undefined) planetSize = [999,999]
        else planetSize = planetSize.split(',')
        var endPosition = landingLoc.split(',')
        for(var x of commands) {
            var whereAmIFacing = compass(cardinalDirections, endPosition[2])
            switch (x) {
                case 'M' :
                    switch (cardinalDirections[whereAmIFacing]) {
                        case 'N' :
                            endPosition[1]++
                            if(endPosition[1]>planetSize[1])endPosition[1]=1
                            break
                        case 'E' :
                            endPosition[0]++
                            if(endPosition[0]>planetSize[0])endPosition[0]=1
                            break
                        case 'S' :
                            endPosition[1]--
                            if(endPosition[1]<planetSize[1])endPosition[1]=planetSize[1]
                            break
                        case 'W' :
                            endPosition[0]--
                            if(endPosition[0]<planetSize[0])endPosition[0]=planetSize[0]
                        default  :
                            break
                    }
                    break
                case 'R' :
                    whereAmIFacing++
                    if(whereAmIFacing>3) whereAmIFacing=0
                    endPosition[2]=cardinalDirections[whereAmIFacing]
                    break
                case 'L' :
                    whereAmIFacing--
                    if(whereAmIFacing<0) whereAmIFacing=3
                    endPosition[2]=cardinalDirections[(whereAmIFacing)]
                    break  
                default  :
                    break
            }
        }
        return endPosition = endPosition.join()
    }
}

function compass(cardinalDirections, landingLocDir) {
    for(var x=0; x<=3;x++) {
        if(cardinalDirections[x]==landingLocDir) return x
    }
    return "face down"
}