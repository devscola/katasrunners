const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');

export function rot13(str) { // LBH QVQ VG!
  let cipheredString = '';

  for (let char of str) {
    if (isLetter(char)) {
      cipheredString += rotateLetter(char)
    } else {
      cipheredString += char;
    }
  }

  return cipheredString;
}

function isLetter(char) {
  return /[A-Z]/.test(char)
}

function rotateLetter(letter) {
  const letterIndex = alphabet.findIndex(char => char === letter);
  
  let rot13Index = (letterIndex + 13) % 26;

  return alphabet[rot13Index];
}
