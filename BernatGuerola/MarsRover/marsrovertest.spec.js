import {Marsrover} from '../src/MarsRover.js'



//'should know its landing position'
describe('Posición de inicio', () => {
    let rover = new Marsrover(5,5,'N');
    it('should return true', () => {
        // Arrange
        const expected_value = "MarsRover [5, 5, N]"

        // Act
        const result = rover.move(rover,"")

        // Assert
        expect(result).toEqual(expected_value)
    })
})

//'should know its final position'
describe('Posición de final', () => {
    let rover = new Marsrover(1,2,'N');
    it('should return true', () => {
        // Arrange
        const expected_value = "MarsRover [1, 5, N]"

        // Act
        const result = rover.move(rover,"MMM")

        // Assert
        expect(result).toEqual(expected_value)
    })
})

//''should move foward when receive the command M''
describe('Hacia delante', () => {
    let rover = new Marsrover(3,2,'N');
    it('should return true', () => {
        // Arrange
        const expected_value = "MarsRover [3, 4, N]"

        // Act
        const result = rover.move(rover,"MM")

        // Assert
        expect(result).toEqual(expected_value)
    })
})

//'should turn 90 degrees right when recives command R'
describe('Girar 90 grados', () => {
    let rover = new Marsrover(2,2,'N');
    it('should return true', () => {
        // Arrange
        const expected_value = "MarsRover [3, 3, E]"

        // Act
        const result = rover.move(rover,"RM")

        // Assert
        expect(result).toEqual(expected_value)
    })
})