class FizzBuzz {
    calculate(number) {
        let result = number.toString() 

        if (this._isFizz(number)) {
            result = 'Fizz'
        }

        if (this._isBuzz(number)) {
            result = 'Buzz'
        }
        if (this._isFizz(number) && this._isBuzz(number)) {
            result = 'FizzBuzz'
        }
        return result
    }

    _isBuzz(number) {
        return number % 5 == 0
    }

    _isFizz(number) {
        return number % 3 == 0
    }
}
module.exports=FizzBuzz