export function convertToRoman(num) {
    num = num.toString();
    var temp = "";
    var indice = 0;
    const numeros_romanos = ["I","X","C","M","!X","!C",,"!M"]; 
    const numeros_romanos_5 = ["V","L","D","!V","!L","!D" ]
    //const numeros_decimales = [1,5,10,50,100,500,1000,5000,10000,50000,100000,500000,1000000];
    for(var x=0; x<=num.length; x++) {
        indice = num.charAt(x);
        indice = parseInt(indice);
        // console.log("indice parseint");
        // console.log(indice);
        if(indice===9) {
            temp += numeros_romanos[num.length - x -1];
            temp += numeros_romanos[num.length - x];
            indice = 0;
        }
        if(indice>=5) {
            temp += numeros_romanos_5[num.length - x -1];
            indice = indice-5;
            if(indice> 0) {
                for(indice; indice>0; indice--) {
                    temp += numeros_romanos[num.length - x -1];
                }
            }
            indice = 0;
        }
        if(indice === 4) {
            temp += numeros_romanos[num.length - x -1];
            temp += numeros_romanos_5[num.length - x -1];
            indice = 0;
        }
        if(indice>0 && indice<=3) {
            for(indice; indice>0; indice--) {
                temp += numeros_romanos[num.length - x -1];
            }
            indice = 0;
        }
        // console.log("temp for");
        // console.log(x);
        // console.log(temp); 
    }
    
    // console.log("temp");
    // console.log(temp);

    
    return temp;
}
