//Obtener datos
function obtenerDatos() {
  let heading = document.getElementById("heading").value;
  let twd = document.getElementById("twd").value;
  let tws = document.getElementById("tws").value;
  let bs = document.getElementById("bs").value;
  let demoraNextMark = document.getElementById("demoraNextMark").value;

  twa = twaCalculate(twd, heading);
  let marcacionNextMark = heading - demoraNextMark;

  let vmg = vmgCalculate(twa, bs);
  let vmc = vmcCalculate(marcacionNextMark, bs);
  
  //let layline_over
  changeInnerHtml("mostrarTWA",twa)
  changeInnerHtml("mostrarVMG",vmg)
  changeInnerHtml("mostrarVMC",vmc)
}

function changeInnerHtml(id, texto){
  const element = document.getElementById(id);
  element.innerHTML = `${texto}`;

}

//Pasar de RADIANES a GRADOS
function grados(radianes) {
  return (radianes * 180) / Math.PI;
}

function radianes(grados) {
  return (grados * Math.PI) / 180;
}

function calcularHipotenusa(lado, angulo) {
  return lado * Math.cos(radianes(angulo));
}
//Calcular TWA
function twaCalculate(twd, heading) {
  var twa = heading - twd;
  //if (twa < 0) twa = twa + 360; //sumamos 360 para trabajar con ángulos positivos
  console.log("TWA = " + twa);
  return twa;
}

//Calcular VMG
function vmgCalculate(twa, bs) {
  //pasar de radianes a grados
  let vmg = calcularHipotenusa(bs, twa);
  console.log("VMG = " + vmg);
  return vmg;
}

//Calcular VMC //ERROR NUEVO
function vmcCalculate(marcacionNextMark, bs) {
  let vmc = calcularHipotenusa(bs, marcacionNextMark);
  console.log("VMC = " + vmc);
  return vmc;
}

