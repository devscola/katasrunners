export class Movements{
    constructor(position_x, position_y){
        this.x = position_x
        this.y = position_y
    }

    currentPosition(){
        return [this.x, this.y]  
    }
    
    move(orientation){
        if(orientation == 'N') this.y += 1
        if(orientation == 'S') this.y -= 1
        if(orientation == 'E') this.x += 1
        if(orientation == 'W') this.x -= 1
    }

}