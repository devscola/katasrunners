import { FizzBuzz } from '../src/FizzBuzz.js'

describe('Example', () => {
    it('Return Fizz when is multiple of 3', () => {
        // Arrange
        const fizzBuzz =  new FizzBuzz()
        const expected_value = "Fizz"

        // Act
        let getString = fizzBuzz.getString(9)
        // Assert
        expect(getString).toBe(expected_value)
    })

    it('Return Buzz when is multiple of 5', () => {
        // Arrange
        const fizzBuzz =  new FizzBuzz()
        const expected_value = "Buzz"

        // Act
        let getString = fizzBuzz.getString(10)
        // Assert
        expect(getString).toBe(expected_value)
    })

    it('Return FizzBuzz when is multiple of 3 and 5', () => {
        // Arrange
        const fizzBuzz =  new FizzBuzz()
        const expected_value = "FizzBuzz"

        // Act
        let getString = fizzBuzz.getString(15)
        // Assert
        expect(getString).toBe(expected_value)
    })

    it('Return string number when is not a multiple of 3 and 5', () => {
        // Arrange
        const fizzBuzz =  new FizzBuzz()
        const expected_value = "2"

        // Act
        let getString = fizzBuzz.getString(2)
        // Assert
        expect(getString).toBe(expected_value)
    })
    it('Return string number when is not a multiple of 3 and 5', () => {
        // Arrange
        const fizzBuzz =  new FizzBuzz()
        const xpected_value = "7"

        // Act
        let getString = fizzBuzz.getString(7)
        // Assert
        expect(getString).toBe(expected_value)
    })
})