from mamba import *
from expects import *

with description('sample') as self:
  with it('true equals true'):
    expect(True).to(equal(True))
