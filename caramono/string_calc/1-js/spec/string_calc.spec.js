import { string_calc } from '../src/string_calc.js'


describe('String calculator', () => {
    it('has to return 0 with an empty string', () => {
        // Arrange
        const expected_value = 0

        // Act
        const result = string_calc('')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('has to return a number', () => {
        // Arrange
        const expected_value = 5

        // Act
        const result = string_calc("5")

        // Assert
        expect(result).toBe(expected_value)
    })

    it('can add two comma-separated numbers', () => {
        // Arrange
        const expected_value = 9
        const  givenString = "5,4"
        // Act
        const result = string_calc(givenString)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('can add any amount of comma-separated numbers', () => {
        // Arrange
        const expected_value = 253
        const  givenString = "5,4,8,4,23,45,66,98"
        // Act
        const result = string_calc(givenString)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('can ignore new line characters', () => {
        // Arrange
        const expected_value = 16
        const  givenString = "5,\n7,4, \n\n\n\n\n\n\n\n\n\n\n\n\n"
        // Act
        const result = string_calc(givenString)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('can use any delimiter for the string', () => {
        // Arrange
        const expected_value = 16
        const givenString = "5;\n7;4; \n\n\n\n\n\n\n\n\n\n\n\n\n"
        const givenDelimiter = ';'

        // Act
        const result = string_calc(givenString, givenDelimiter)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('does not accept a negative number', () => {
        // Arrange
        const expected_value = "negatives not allowed: -1"
        const  givenString = "5,\n7,4, \n\n\n\n\n\n\n\n\n\n\n\n\n,-1"
        // Act
        const result = string_calc(givenString)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('does not accept any amount of negative numbers', () => {
        // Arrange
        const expected_value = "negatives not allowed: -1 -8 -357"
        const  givenString = "5,\n7,4, \n\n\n\n\n\n\n\n\n\n\n\n\n,-1,-8,-357"
        // Act
        const result = string_calc(givenString)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('does not add any number bigger than 1000', () => {
        // Arrange
        const expected_value = 16
        const  givenString = "5,7,4,1800,7439857345"
        // Act
        const result = string_calc(givenString)

        // Assert
        expect(result).toBe(expected_value)
    })
})
