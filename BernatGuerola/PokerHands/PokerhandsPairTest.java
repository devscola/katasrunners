import static org.junit.Assert.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class PokerhandsPairTest{
	Pokerhands tester = new Pokerhands();
	
  @DisplayName( "devuelveMayorDeDosCartas" )
  @ParameterizedTest( name = "{index}: ({0} + {1}) => {2})" )
  @MethodSource( "localParameters" )
  void test( String first, String second, String resul ){
	String obtenido = tester.devuelveMasAlta(first, second);
	String esperado = resul; 
	assertEquals(esperado,obtenido);
  }

  static Stream< Arguments > localParameters()
  {
    return Stream.of(
        Arguments.of( "4", "J", "J"),
        Arguments.of( "K", "9", "K"),
        Arguments.of( "5", "3", "3")
    );
  }
}

