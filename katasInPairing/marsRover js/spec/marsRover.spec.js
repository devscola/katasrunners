import { Rover } from '../src/marsRover.js'

describe('Rover', () => {
    it('knows her landing position', () => {
        // Arrange
        const expected_value = [5, 5, 'N']
        const rover = new Rover([5, 5, 'N'])

        // Act
        const result = rover.landingPosition()

        // Assert
        expect(result).toEqual(expected_value)
    })
})

describe('Rover turn', () => {
    it('turn west from north when left (L) command', () => {
        // Arrange
        const expected_value = [5, 5, 'W']
        const rover = new Rover([5, 5, 'N'])
        rover.command(['L'])

        const result = rover.currentPosition()

        expect(result).toEqual(expected_value)
    })

    it('turn south from north when left (L) 2 times command', () => {
        // Arrange
        const expected_value = [5, 5, 'S']
        const rover = new Rover([5, 5, 'N'])
        rover.command(['L', 'L'])

        const result = rover.currentPosition()

        expect(result).toEqual(expected_value)
    })

    it('turn west from north when left (L) 5 times command', () => {
        // Arrange
        const expected_value = [5, 5, 'W']
        const rover = new Rover([5, 5, 'N'])
        rover.command(['L', 'L', 'L', 'L', 'L'])

        const result = rover.currentPosition()

        expect(result).toEqual(expected_value)
    })

    it('turn east from north when right (R) 1 times command', () => {
        // Arrange
        const expected_value = [5, 5, 'E']
        const rover = new Rover([5, 5, 'N'])
        rover.command(['R'])

        const result = rover.currentPosition()

        expect(result).toEqual(expected_value)
    })

    it('turn east from north when right (R) 5 times command', () => {
        // Arrange
        const expected_value = [5, 5, 'E']
        const rover = new Rover([5, 5, 'N'])
        rover.command(['R', 'R', 'R', 'R', 'R'])

        const result = rover.currentPosition()

        expect(result).toEqual(expected_value)
    })
})

describe('Rover move', () => {

    it('to front when recive an m command', () => {
        // Arrange
        const expected_value = [5, 6, 'N']
        const rover = new Rover([5, 5, 'N'])
        rover.command(['M'])
        // Act
        const result = rover.currentPosition()

        // Assert
        expect(result).toEqual(expected_value)
    })

    it('two times', () => {
        // Arrange
        const expected_value = [5, 7, 'N']
        const rover = new Rover([5, 5, 'N'])
        rover.command(['M', 'M'])
        // Act
        const result = rover.currentPosition()

        // Assert
        expect(result).toEqual(expected_value)
    })
    
    it('when rover has south orientation', () => {
        // Arrange
        const expected_value = [5, 4, 'S']
        const rover = new Rover([5, 5, 'S'])
        rover.command(['M'])

        const result = rover.currentPosition()

        expect(result).toEqual(expected_value)
    })

    it('when rover is east orientation', () => {
        // Arransge
        const expected_value = [8, 7, 'E']
        const rover = new Rover([7, 7, 'E'])
        rover.command(['M'])

        const result = rover.currentPosition()

        expect(result).toEqual(expected_value)
    })

    it('when rover is west orientation', () => {
        // Arransge
        const expected_value = [6, 7, 'W']
        const rover = new Rover([7, 7, 'W'])
        rover.command(['M'])

        const result = rover.currentPosition()

        expect(result).toEqual(expected_value)
    })
})
