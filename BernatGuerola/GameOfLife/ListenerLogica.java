package principal;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ListenerLogica implements ActionListener {

	// 1. Any live cell with fewer than two live neighbours dies, as if caused by
	// underpopulation.
	// 2. Any live cell with more than three live neighbours dies, as if by
	// overcrowding.
	// 3. Any live cell with two or three live neighbours lives on to the next
	// generation.
	// 4. Any dead cell with exactly three live neighbours becomes a live cell.

	public Color colordonat(int cont, Color inici) {
		Color colorfinal = Color.LIGHT_GRAY;
		if ((cont == 1 || cont == 0) && inici == Color.GREEN) {
			colorfinal = Color.LIGHT_GRAY;
		}
		if ((cont == 4 || cont == 5 || cont == 6 || cont == 7 || cont == 8) && inici == Color.GREEN) {
			colorfinal = Color.LIGHT_GRAY;
		}
		if ((cont == 2 || cont == 3) && inici == Color.GREEN) {
			colorfinal = Color.GREEN;
		}
		if (cont == 3 && inici == Color.LIGHT_GRAY) {
			colorfinal = Color.GREEN;
		}
		return colorfinal;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Utils.intents += 1;
		Disseny.etiqueta.setText("Generation n�: " + Utils.intents + " ");

		for (int i = 0; i < Utils.DIMENSION; i++) {
			for (int j = 0; j < Utils.DIMENSION; j++) {
				int contVecinos=0;
				Color inici = (Utils.matriu[i][j]).getBackground();
				
							
				if ((i - 1) >= 0 && (j - 1) >= 0) {
					if (((Utils.matriu[i - 1][j - 1]).getBackground()) == Color.GREEN) {
						contVecinos++;
					}
					;
				}
				if ((i - 1) >= 0) {
					if (((Utils.matriu[i - 1][j]).getBackground()) == Color.GREEN) {
						contVecinos++;
					}
					;
				}
				if ((i - 1) >= 0 && ((j + 1) < 8)) {
					if (((Utils.matriu[i - 1][j + 1]).getBackground()) == Color.GREEN) {
						contVecinos++;
					}
					;
				}
				if ((j - 1) >= 0) {
					if (((Utils.matriu[i][j - 1]).getBackground()) == Color.GREEN) {
						contVecinos++;
					}
					;
				}
				if ((j + 1) < 8) {
					if (((Utils.matriu[i][j + 1]).getBackground()) == Color.GREEN) {
						contVecinos++;
					}
					;
				}
				if ((i + 1) < 8 && (j - 1) >= 0) {
					if (((Utils.matriu[i + 1][j - 1]).getBackground()) == Color.GREEN) {
						contVecinos++;
					}
					;
				}
				if ((i + 1) < 8) {
					if (((Utils.matriu[i + 1][j]).getBackground()) == Color.GREEN) {
						contVecinos++;
					}
					;
				}
				if ((i + 1) < 8 && (j + 1) < 8) {
					if (((Utils.matriu[i + 1][j + 1]).getBackground()) == Color.GREEN) {
						contVecinos++;
					}
					;
				}

				(Utils.matriu[i][j]).setBackground(colordonat(contVecinos, inici));
				System.out.print(contVecinos);
			}
		}
	}
}
