// import {FizzBuzz} from '../src/FizzBuzz.js'

describe("Player", () => {

  xit("is a list numbers from 1 to 100 ", () => {

    const fizzbuzz = new FizzBuzz()
    const result = fizzbuzz.getNumberEqual()
    expect(result.length).toEqual(101)
    expect(result[0]).toEqual(1)
    expect(result[99]).toEqual(100)
    console.log(result)
  })

  it('If the number is divisible by 3, write "Fizz" instead', () => {
    //  Arrange
    const fizzBuzz = new FizzBuzz()
    const expectedValue = 'fizz'

    //  Act 
    const result = fizzBuzz.getNumberEqual(3)
    
   
    expect(result).toEqual(expectedValue)
  })

  it('If the number is divisible by 5, write "Buzz" instead', () => {
    //  Arrange
    const fizzBuzz = new FizzBuzz()
    const expectedValue = 'buzz'

    //  Act 
    const result = fizzBuzz.getNumberEqual(25)
    
   
    expect(result).toEqual(expectedValue)
  })

  it('If the number is divisible by 3 and 5, write "FizzBuzz" instead', () => {
    //  Arrange
    const fizzBuzz = new FizzBuzz()
    const expectedValue = 'fizzbuzz'

    //  Act 
    const result = fizzBuzz.getNumberEqual(15)
    
   
    expect(result).toEqual(expectedValue)
  })

  it('If the number is not divisible neither by 3 and 5, write the string representation of the number', () => {
    //  Arrange
    const fizzBuzz = new FizzBuzz()
    const expectedValue = '2'

    //  Act 
    const result = fizzBuzz.getNumberEqual(2)
    
   
    expect(result).toEqual(expectedValue)
  })

  it('if it has a 3 in it is Fizz.', () => {
    //  Arrange
    const fizzBuzz = new FizzBuzz()
    const expectedValue = 'fizz'

    //  Act 
    const result = fizzBuzz.getNumberEqual(13)
    
   
    expect(result).toEqual(expectedValue)
  })

  it('if it has a 5 in it is Buzz.', () => {
    //  Arrange
    const fizzBuzz = new FizzBuzz()
    const expectedValue = 'buzz'

    //  Act 
    const result = fizzBuzz.getNumberEqual(52)
    
   
    expect(result).toEqual(expectedValue)
  })


})



// Código



class FizzBuzz {
 
  getNumberEqual(num){
    
    if (this.isFizzBuzz(num)) {
      return 'fizzbuzz'
    }

    if (this.isFizz(num)) {
      return 'fizz'
    }

    if (this.isBuzz(num)) {
      return 'buzz'
    }

    if (!this.isFizzBuzz(num)) {
      return num.toString()
    }
  }
  
  isBuzz(num) {
    const convertToString = num.toString()
    return num % 5 == 0 || convertToString.includes('5')
  }

  isFizz(num) {
    const convertToString = num.toString()
    return num %3 == 0 || convertToString.includes('3')
  }

  isFizzBuzz(num) {
    return this.isFizz(num)  && this.isBuzz(num)
  }

}

