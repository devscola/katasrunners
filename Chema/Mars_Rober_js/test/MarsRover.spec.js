const MarsRover = require('../src/MarsRover.js')

describe('MarsRover', () => {

    it('return landing initial position', () => {
        //Arrange(prepación)
        const landing = { y: 5, x: 5, m: 'N' }
        const command = "";
        const rover = new MarsRover(landing);
        //Acc(poner a prueba nuestra app)
        const roverPosition = rover.move(landing, command)
        //Assert(comprobar que funciona)
        expect(roverPosition).toStrictEqual(landing)
    })

    it('knows the final position', () => {
        //Arrange(prepación)
        const initialPosition = { y: 1, x: 2, m: 'N' }
        const command = ['M', 'M', 'M'];
        const finalPosition = { y: 1, x: 5, m: 'N' }
        let rover = new MarsRover(initialPosition);
        //Acc(poner a prueba nuestra app)
        let position = rover.move(initialPosition, command)
        //Assert(comprobar que funciona)
        expect(position).toStrictEqual(finalPosition)
    })

    it('When the rover recives the command "R" it turns 90 degrees right', () => {
        //Arrange(prepación)
        const initialPosition = { y: 1, x: 2, m: 'N' }
        const command = ['R', 'R', 'R'];
        const finalPosition = { y: 1, x: 2, m: 'O' }
        let rover = new MarsRover(initialPosition);
        //Acc(poner a prueba nuestra app)
        let position = rover.move(initialPosition, command)
        //Assert(comprobar que funciona)
        expect(position).toStrictEqual(finalPosition)
    })

    it('When the rover recives the command "L" it turns 90 degrees left', () => {
        //Arrange(prepación)
        const initialPosition = { y: 1, x: 2, m: 'N' }
        const command = ['L', 'L', 'L'];
        const finalPosition = { y: 1, x: 2, m: 'E' }
        let rover = new MarsRover(initialPosition);
        //Acc(poner a prueba nuestra app)
        let position = rover.move(initialPosition, command)
        //Assert(comprobar que funciona)
        expect(position).toStrictEqual(finalPosition)
    })

    it('If the rover passes the world edge, it appears in the opposite world position', () => {
        //Arrange(prepación)
        const initialPosition = { y: 1, x: 1, m: 'N' }
        const command = ['M', 'M', 'M', 'M', 'M'];
        const finalPosition = { y: 1, x: 1, m: 'N' }
        let rover = new MarsRover(initialPosition);
        //Acc(poner a prueba nuestra app)
        let position = rover.move(initialPosition, command)
        //Assert(comprobar que funciona)
        expect(position).toStrictEqual(finalPosition)
    })
})

