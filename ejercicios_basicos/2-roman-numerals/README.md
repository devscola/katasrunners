# Números romanos

Convierte el número dado a un número romano.

Todos los números romanos deben devolverse en mayúsculas.

## Instalación y uso

### Javascript

#### Ejecutando los tests

`npm test`

#### Instalación

- Primero instala NodeJS. Puedes encontrar los detalles [en su web](https://nodejs.org/en/). Esto instalará `Node` y `npm`.
- Instala las dependencias utilizando `npm`: `npm install`

### Python

#### Ejecutando los tests

`python3 -m unittest test_example.py`

#### Instalación

- Instala Python 3. Puedes encontrar los detalles [en su web](https://www.python.org/downloads/).

### Ruby

#### Ejecutando los tests

`ruby test_roman_numerals.rb`

#### Instalación

- Instala el intérprete de Ruby. Puedes encontrar los detalles [en su web](https://www.ruby-lang.org/es/)
