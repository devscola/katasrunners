export class PokerHands {
    constructor(firstHand, secondHand){
        this.firstHand = firstHand
        this.secondHand = secondHand
    }
    calculateWinner(){

        const firstHandHighestCard = this.calculateHighestCardInHand(this.firstHand)
        const secondtHandHighestCard = this.calculateHighestCardInHand(this.secondHand)

        if (firstHandHighestCard > secondtHandHighestCard){
        return "The winner is firstdHand"
        }

        return "The winner is secondHand"
    }

    calculateHighestCardInHand(hand){
        const onlyHandNumbers = hand.map(el => el[0])
        const cardValues = {
            "J" : 11,
            "Q" : 12,
            "K" : 13,
            "A" : 14
        }

        const handValues = onlyHandNumbers.map(el => {
            if (Number.isInteger(el)){
                return el
            }

            return cardValues[el]
        })

        return handValues.reduce((acc, el) => {
            if (acc > el){
                return acc
            }
            
            return el
        })
    }
}
