class MarsRover {

    constructor(landing) {
        this.position = landing;

    }

    getPosition() {
        return this.position;
    }

    move(landing, command) {
        const wordPosition = [5, 5]
        if (command === "" || command.length === wordPosition[1]) return this.getPosition()
        let flag = 1
        for (let i = 0; i < command.length; i++) {
            if (command[i] === 'M') {
                this.position.x++
            }

            if (command[i] === 'R') {
                this.turn_right(flag);
                flag++
            }

            if (command[i] === 'L') {
                this.turn_left(flag);
                flag++
            }
        }

        return this.position;

    }
    turn_right(flag) {
        const turn = ['N', 'E', 'S', 'O'];
        this.position.m = turn[flag]
    }

    turn_left(flag) {
        const turn = ['N', 'O', 'S', 'E'];
        this.position.m = turn[flag]
    }

}

module.exports = MarsRover