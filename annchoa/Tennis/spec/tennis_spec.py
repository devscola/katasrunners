from mamba import *
from expects import *
from tennis import *

with description('Tennis') as self:
    with it('players start with 0 points'):
        tennisScore = TennisScore()
        points_per_player = tennisScore.points()
        expect(points_per_player).to(equal((0, 0)))

    with it('winner has 4 points and 2 more than the opponent') as self:
        tennisScore = TennisScore()
        tennisScore.player1 = 4
        tennisScore.player2 = 2
        getScore = tennisScore.getGameScore()
        expect(getScore).to(equal('Winner: player1'))

    with it('winner has 4 points and 2 more than the opponent') as self:
        tennisScore = TennisScore()
        tennisScore.player1 = 1
        tennisScore.player2 = 4
        getScore = tennisScore.getGameScore()
        expect(getScore).to(equal('Winner: player2'))

    with it('no winner because nobody has at least 4. Should return score name') as self:
        tennisScore = TennisScore()
        tennisScore.player1 = 3
        tennisScore.player2 = 1
        getScore = tennisScore.getGameScore()
        expect(getScore).to(equal(('forty', 'fifteen')))

    with it('Deuce when same score at least 3 points or up but equal') as self:
        tennisScore = TennisScore()
        tennisScore.player1 = 3
        tennisScore.player2 = 3
        getScore = tennisScore.getGameScore()
        expect(getScore).to(equal('Deuce'))

    with it('Advantage when somebody is 1 point more after a deuce') as self:
        tennisScore = TennisScore()
        tennisScore.player1 = 3
        tennisScore.player2 = 4
        getScore = tennisScore.getGameScore()
        expect(getScore).to(equal('Advantage: player2'))

    with it('wins a set at least 6 games and 2 more than the oponent. Winner player 2') as self:
        tennisScore = TennisScore()
        tennisScore.player1 = 3
        tennisScore.player2 = 6
        getSetScore = tennisScore.getSetScore()
        expect(getSetScore).to(equal('Player 2 won a set'))

    with it('wins a set at least 6 games and 2 more than the oponent. Winner player 1') as self:
        tennisScore = TennisScore()
        tennisScore.player1 = 6
        tennisScore.player2 = 2
        getSetScore = tennisScore.getSetScore()
        expect(getSetScore).to(equal('Player 1 won a set'))

    with it('additional game when is 6 and 5. Additional Game') as self:
        tennisScore = TennisScore()
        tennisScore.player1 = 5
        tennisScore.player2 = 6
        getSetScore = tennisScore.getSetScore()
        expect(getSetScore).to(equal('Additional game'))

    with it('7-5 wins a game') as self:
        tennisScore = TennisScore()
        tennisScore.player1 = 7
        tennisScore.player2 = 5
        getSetScore = tennisScore.getSetScore()
        expect(getSetScore).to(equal('Player 1 winner'))

    with it('tie-break') as self:
        tennisScore = TennisScore()
        tennisScore.player1 = 6
        tennisScore.player2 = 6
        getSetScore = tennisScore.getSetScore()
        expect(getSetScore).to(equal('Tie-break'))

    with it('After tie-break winner can have 7 and the other one 6. Player 1') as self:
        tennisScore = TennisScore()
        tennisScore.player1 = 6
        tennisScore.player2 = 7
        getSetScore = tennisScore.getSetScore()
        expect(getSetScore).to(equal('Player 2 winner'))

    with it('After tie-break winner can have 7 and the other one 6. Player 2') as self:
        tennisScore = TennisScore()
        tennisScore.player1 = 7
        tennisScore.player2 = 6
        getSetScore = tennisScore.getSetScore()
        expect(getSetScore).to(equal('Player 1 winner'))

    with it('Is a match. Need best of 3 to win') as self:
        tennisScore = tennisScore()
        tennisScore.player1 = 2
        tennisScore.player2 = 1
        getMatch = tennisScore.getMatch()
        expect(getMatch).to(equal('Player 1 win the match'))
