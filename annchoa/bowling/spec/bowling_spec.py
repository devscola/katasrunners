from mamba import *
from expects import *
from bowling import *

with description('Bowling') as self:
    with it('simple return'):
        bowling = Bowling([[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]])
        bowling_score = bowling.bowling_score()
        expect(bowling_score).to(equal(0))

    with it('first frame with point'):
        bowling = Bowling([[3, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]])
        bowling_score = bowling.bowling_score()
        expect(bowling_score).to(equal(3))

    with it('first frame with point'):
        bowling = Bowling([[3, 3], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]])
        bowling_score = bowling.bowling_score()
        expect(bowling_score).to(equal(6))

    with it('first and second frame with point'):
        bowling = Bowling([[3, 3], [5, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]])
        bowling_score = bowling.bowling_score()
        expect(bowling_score).to(equal(11))

    with it('spare on first frame'):
        bowling = Bowling([[3, "/"], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]])
        bowling_score = bowling.bowling_score()
        expect(bowling_score).to(equal(10))

    with it('spare and points'):
        bowling = Bowling([[3, "/"], [0, 0], [6, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]])
        bowling_score = bowling.bowling_score()
        expect(bowling_score).to(equal(16))

    with it('spare and points after that'):
        bowling = Bowling([[3, "/"], [3, 0], [6, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]])
        bowling_score = bowling.bowling_score()
        expect(bowling_score).to(equal(22))

    with it('two spares'):
        bowling = Bowling([[3, "/"], [3, "/"], [6, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]])
        bowling_score = bowling.bowling_score()
        expect(bowling_score).to(equal(35))

    with it('just one strike'):
        bowling = Bowling([[3, "/"], [0, 0], ["X"], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]])
        bowling_score = bowling.bowling_score()
        expect(bowling_score).to(equal(20))

    with it('strike add next two turns'):
        bowling = Bowling([[3, "/"], [0, 0], ["X"], [5, 4], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]])
        bowling_score = bowling.bowling_score()
        expect(bowling_score).to(equal(38))

    with it('strike and spare'):
        bowling = Bowling([[3, "/"], [0, 0], ["X"], [5, "/"], [4, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]])
        bowling_score = bowling.bowling_score()
        expect(bowling_score).to(equal(48))

    with it('strike and spare everywhere'):
        bowling = Bowling([[3, "/"], [5, "/"], ["X"], [5, "/"], [4, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]])
        bowling_score = bowling.bowling_score()
        expect(bowling_score).to(equal(73))

    with it('three strikes'):
        bowling = Bowling([["X"], ["X"], ["X"], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]])
        bowling_score = bowling.bowling_score()
        expect(bowling_score).to(equal(60))

    with it('three and spares'):
        bowling = Bowling([["X"], ["X"], ["X"], [5, "/"], ["X"], [3, "/"], ["X"], [0, 0], [0, 0], [0, 0]])
        bowling_score = bowling.bowling_score()
        expect(bowling_score).to(equal(145))

#este y el del final con 3 lanzamientos
