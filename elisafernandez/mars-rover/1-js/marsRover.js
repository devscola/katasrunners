class MarsRover {
    constructor(initialPosition) {
        this.position = initialPosition;
    }

    cardinalDirections = ['N', 'E', 'S', 'W'];
    worldDimensions;

    executeCommand(command) {
        for (let char of command) {
            if (char === 'm') {
                this.moveForward();
                this.goOverBorder();
            }
            if (char === 'l') {
                this.turnLeft();
            }
            if (char === 'r') {
                this.turnRight();
            }
        }
    }

    moveForward() {
        if (this.position.d === 'N') {
            this.position.y += 1;
        }
        else if (this.position.d === 'E') {
            this.position.x += 1;
        }
        else if (this.position.d === 'S') {
            this.position.y -= 1;
        }
        else if (this.position.d === 'W') {
            this.position.x -= 1;
        }
    }

    goOverBorder() {
        if (this.worldDimensions) {
            if (this.position.y > this.worldDimensions.y) {
                this.position.y = 1;
            }
            if (this.position.x > this.worldDimensions.x) {
                this.position.x = 1;
            }
            if (this.position.y < 1) {
                this.position.y = this.worldDimensions.y;
            }
            if (this.position.x < 1) {
                this.position.x = this.worldDimensions.x;
            }
        }
    }

    turnLeft() {
        let cardinalIndex = this.cardinalDirections.indexOf(this.position.d);
        cardinalIndex--;
        if (cardinalIndex < 0) {
            cardinalIndex = 3;
        }
        this.position.d = this.cardinalDirections[cardinalIndex];
    }

    turnRight() {
        let cardinalIndex = this.cardinalDirections.indexOf(this.position.d);
        cardinalIndex++;
        if (cardinalIndex > 3) {
            cardinalIndex = 0;
        }
        this.position.d = this.cardinalDirections[cardinalIndex];
    }

}

module.exports = MarsRover