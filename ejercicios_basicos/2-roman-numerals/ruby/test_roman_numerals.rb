require_relative 'roman_numerals'
require 'test/unit'

class TestRomanNumerals < Test::Unit::TestCase
  def test_a_convert_single_number
    arabic_number = 1 
    expected_result = 'I'

    actual_result = convert_to_roman(arabic_number)

    assert_equal(expected_result, actual_result)
  end

  def test_b_uses_same_symbol_multiple_times
    arabic_number = 3
    expected_result = 'III'

    actual_result = convert_to_roman(arabic_number)

    assert_equal(expected_result, actual_result)
  end

  def test_c_combines_two_symbols_for_numbers_smaller_than_nine
    arabic_number = 8
    expected_result = 'VIII'

    actual_result = convert_to_roman(arabic_number)

    assert_equal(expected_result, actual_result)
  end

  def test_d_combines_three_symbols_for_numbers_smaller_than_forty
    arabic_number = 38
    expected_result = 'XXXVIII'

    actual_result = convert_to_roman(arabic_number)

    assert_equal(expected_result, actual_result)
  end

  def test_e_combines_all_symbols_to_convert_numbers
    arabic_number = 3256
    expected_result = 'MMMCCLVI'

    actual_result = convert_to_roman(arabic_number)

    assert_equal(expected_result, actual_result)
  end

  def test_f_ensures_a_symbol_is_not_repeated_more_than_three_times
    arabic_number = 4
    expected_result = 'IV'

    actual_result = convert_to_roman(arabic_number)

    assert_equal(expected_result, actual_result)
  end
end
