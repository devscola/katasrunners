var {Shop, Item} = require('../src/gilded_rose.js');
describe("Gilded Rose", function() {

  it("should decrease quality and sellIn of a common item", function() {
    const gildedRose = new Shop([ new Item("common", 1, 1) ]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toEqual("common");
    expect(items[0].sellIn).toEqual(0);
    expect(items[0].quality).toEqual(0);
  });

  it("should decrease sellIn and increment quality of Aged Brie", function() {
    const gildedRose = new Shop([ new Item("Aged Brie", 1, 1) ]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toEqual("Aged Brie");
    expect(items[0].sellIn).toEqual(0);
    expect(items[0].quality).toEqual(2);
  });

  it("stable quality and sellIn of Sulfuras, Hand of Ragnaros", function() {
    const gildedRose = new Shop([ new Item("Sulfuras, Hand of Ragnaros", 1, 1) ]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toEqual("Sulfuras, Hand of Ragnaros");
    expect(items[0].sellIn).toEqual(1);
    expect(items[0].quality).toEqual(1);
  });

  it("decrese quality and sellIn of Backstage passes", function() {
    const gildedRose = new Shop([ new Item("Backstage passes to a TAFKAL80ETC concert", 0, 0) ]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toEqual("Backstage passes to a TAFKAL80ETC concert");
    expect(items[0].sellIn).toEqual(-1);
    expect(items[0].quality).toEqual(0);
  });

  it("should increment quality when sellIn of Backstage passes >= 10", function() {
    const gildedRose = new Shop([ new Item("Backstage passes to a TAFKAL80ETC concert", 20, 1) ]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toEqual("Backstage passes to a TAFKAL80ETC concert");
    expect(items[0].sellIn).toEqual(19);
    expect(items[0].quality).toEqual(2);
  });

  it("should increment quality when sellIn of Backstage passes <= 10", function() {
    const gildedRose = new Shop([ new Item("Backstage passes to a TAFKAL80ETC concert", 9, 1) ]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toEqual("Backstage passes to a TAFKAL80ETC concert");
    expect(items[0].sellIn).toEqual(8);
    expect(items[0].quality).toEqual(3);
  });

  it("should increment quality when sellIn of Backstage passes <= 5", function() {
    const gildedRose = new Shop([ new Item("Backstage passes to a TAFKAL80ETC concert", 4, 1) ]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toEqual("Backstage passes to a TAFKAL80ETC concert");
    expect(items[0].sellIn).toEqual(3);
    expect(items[0].quality).toEqual(4);
  });

  it("quality never less than 0", function() {
    const gildedRose = new Shop([ new Item("common", 9, 0) ]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toEqual("common");
    expect(items[0].sellIn).toEqual(8);
    expect(items[0].quality).toEqual(0);
  });

  it("quality is never over 50", function() {
    const gildedRose = new Shop([ new Item("Aged Brie", 9, 50) ]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toEqual("Aged Brie");
    expect(items[0].sellIn).toEqual(8);
    expect(items[0].quality).toEqual(50);
  });
});
