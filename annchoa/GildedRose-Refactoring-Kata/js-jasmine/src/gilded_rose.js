const AGED_BRIE = 'Aged Brie'
const TICKET = 'Backstage passes to a TAFKAL80ETC concert'
const SULFURAS = 'Sulfuras, Hand of Ragnaros'
const MAX_QUALITY = 50
const MIN_QUALITY = 0

class Item {
  constructor(name, sellIn, quality) {
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }

  increaseQuality() {
    if (this.isMaxQuality()) {
      return
    }
    this.quality++;
  }
  decreaseQuality() {
    if (this.isMinQuality()) {
      return
    }
    this.quality--;
  }

  isMaxQuality() {
    return this.quality == MAX_QUALITY
  }
  isMinQuality() {
    return this.quality == MIN_QUALITY
  }

  decreaseSellIn() {
    this.sellIn--
  }

  updateQuality() {
  }
}

class Sulfuras extends Item {}

class CommonItem extends Item {
  updateQuality() {
    this.decreaseSellIn()
    this.decreaseQuality()
  }
}

class Brie extends Item {
  updateQuality() {
    this.decreaseSellIn()
    this.increaseQuality()
    if (this.sellIn < 0) {
      this.increaseQuality();
    }
  }
}

class BackstageTicket extends Item {
  updateQuality() {
    this.decreaseSellIn()
    if (this.sellIn < 0) {
      this.resetQuality()
      return
    }
    this.increaseQuality();
    if (this.sellIn < 11) {
      this.increaseQuality()
    }
    if (this.sellIn < 6) {
      this.increaseQuality()
    }
  }

  resetQuality() {
    this.quality = MIN_QUALITY
  }
}

class ItemFactory {
  static create(item) {
    if (ItemFactory.isSulfuras(item.name)) {
      return new Sulfuras(item.name, item.sellIn, item.quality)
    }
    if (ItemFactory.isBrie(item.name)) {
      return new Brie(item.name, item.sellIn, item.quality)
    }
    if (ItemFactory.isTicket(item.name)) {
      return new BackstageTicket(item.name, item.sellIn, item.quality)
    }
    return new CommonItem(item.name, item.sellIn, item.quality)
  }

  static isSulfuras(name) {
    return name == SULFURAS
  }
  static isBrie(name) {
    return name == AGED_BRIE
  }
  static isTicket(name) {
    return name == TICKET
  }
}

class Shop {
  constructor(items = []) {
    this.items = items.map(item => ItemFactory.create(item))
  }

  updateQuality() {
    this.items.map((item) => {
      item.updateQuality()
    })

    return this.items;
  }
}

module.exports = {
  Item,
  Shop
// }
