import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Disseny {
	
	JButton btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn0,btnClear,btnIntro;
	static String textpantalla="";
	JTextField pantalla = new JTextField(20);

	public class ActionListenerTerminal implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object source = e.getSource();
			
			if (source==btn1) {String aux=btn1.getText();textpantalla+=aux;pantalla.setText(textpantalla);}
			if (source==btn2) {String aux=btn2.getText();textpantalla+=aux;pantalla.setText(textpantalla);}
			if (source==btn3) {String aux=btn3.getText();textpantalla+=aux;pantalla.setText(textpantalla);}
			if (source==btn4) {String aux=btn4.getText();textpantalla+=aux;pantalla.setText(textpantalla);}
			if (source==btn5) {String aux=btn5.getText();textpantalla+=aux;pantalla.setText(textpantalla);}
			if (source==btn6) {String aux=btn6.getText();textpantalla+=aux;pantalla.setText(textpantalla);}
			if (source==btn7) {String aux=btn7.getText();textpantalla+=aux;pantalla.setText(textpantalla);}
			if (source==btn8) {String aux=btn8.getText();textpantalla+=aux;pantalla.setText(textpantalla);}
			if (source==btn9) {String aux=btn9.getText();textpantalla+=aux;pantalla.setText(textpantalla);}
			if (source==btn0) {String aux=btn0.getText();textpantalla+=aux;pantalla.setText(textpantalla);}
			if (source==btnClear) {textpantalla="";pantalla.setText(textpantalla);}
			if (source==btnIntro) {enterPrecio();}
		}

		private void enterPrecio() {
			String s= "Precio art�culo. Introduzca monedas entregadas";
			Utils.missatge.setText(s);
			System.out.println("M�todo calcular precio");	
		}
	}

	@SuppressWarnings({ "removal", "deprecation" })
	public void board(Datos caixa) {
		
		JFrame f = new JFrame();
		f.setTitle("Cash Register");
		f.setSize(365, 700);
		f.setResizable(false);
		
		JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        f.add(panel);
		
		JPanel ventana = new JPanel();
		ventana.setLayout(null);
		ventana.setBackground(Color.cyan);
		
		pantalla.setHorizontalAlignment(JTextField.RIGHT);
		pantalla.setEditable(false);
		Font fuente=new Font("Dialog", Font.BOLD, 18);
		pantalla.setFont(fuente) ;		
		pantalla.setText(textpantalla);
		pantalla.setBounds(15, 15, 320, 30);
		ventana.add(pantalla);
		
		Utils.missatge = new JLabel();
		Utils.missatge.setBounds(15, 50, 320, 25);
		Utils.missatge.setText(Utils.msginici);		
		ventana.add(Utils.missatge);
		
		JPanel panelbtn = new JPanel();
		panelbtn.setBackground(Color.gray);
		panelbtn.setLayout(new GridLayout(4, 3));
		
		btn1 = new JButton("1"); btn1.addActionListener(new ActionListenerTerminal());
		btn2 = new JButton("2");btn2.addActionListener(new ActionListenerTerminal());
		btn3 = new JButton("3");btn3.addActionListener(new ActionListenerTerminal());
		btn4 = new JButton("4");btn4.addActionListener(new ActionListenerTerminal());
		btn5 = new JButton("5");btn5.addActionListener(new ActionListenerTerminal());
		btn6 = new JButton("6");btn6.addActionListener(new ActionListenerTerminal());
		btn7 = new JButton("7");btn7.addActionListener(new ActionListenerTerminal());
		btn8 = new JButton("8");btn8.addActionListener(new ActionListenerTerminal());
		btn9 = new JButton("9");btn9.addActionListener(new ActionListenerTerminal());
		btn0 = new JButton("0");btn0.addActionListener(new ActionListenerTerminal());
		btnClear = new JButton("CLEAR");btnClear.addActionListener(new ActionListenerTerminal());
		btnIntro = new JButton("ENTER");btnIntro.addActionListener(new ActionListenerTerminal());
		
		panelbtn.add(btn1);
		panelbtn.add(btn2);
		panelbtn.add(btn3);
		panelbtn.add(btn4);
		panelbtn.add(btn5);
		panelbtn.add(btn6);
		panelbtn.add(btn7);
		panelbtn.add(btn8);
		panelbtn.add(btn9);
		panelbtn.add(btn0);
		panelbtn.add(btnClear);
		panelbtn.add(btnIntro);
		
		JPanel caja = new JPanel();
		caja.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		caja.setBackground(Color.cyan);
		caja.setLayout(new GridLayout(12, 4));
		
		Font fntcaixa=new Font("Dialog", Font.ITALIC, 10);
		Font fntdevol=new Font("Dialog", Font.PLAIN, 10);
		Font fntdado=new Font("Dialog", Font.BOLD,10);
				
		JLabel lblonehundred = new JLabel("One-hundred Dollars");
		Utils.txtonehundred = new JTextField(5);
		Utils.txtonehundred.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtonehundred.setText(""+caixa.getOnehundred());
		Utils.txtonehundred.setEditable(false);
		Utils.txtonehundred.setFont(fntcaixa);
		Utils.txtonehundreddado = new JFormattedTextField (new Integer (0));
		Utils.txtonehundreddado.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtonehundreddado.setEditable(true);
		Utils.txtonehundreddado.setFont(fntdado);	
		Utils.txtonehundreddev = new JTextField(5);
		Utils.txtonehundreddev.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtonehundreddev.setEditable(false);
		Utils.txtonehundreddev.setFont(fntdevol);
		
		JLabel lbltwenty = new JLabel("Twenty Dollars");
		Utils.txtenty = new JTextField(5);
		Utils.txtenty.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtenty.setText(""+caixa.getTwenty());
		Utils.txtenty.setEditable(false);
		Utils.txtenty.setFont(fntcaixa);
		Utils.txtentydado = new JFormattedTextField (new Integer (0));
		Utils.txtentydado.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtentydado.setEditable(true);
		Utils.txtentydado.setFont(fntdado);				
		Utils.txtentydev = new JTextField(5);
		Utils.txtentydev.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtentydev.setEditable(false);
		Utils.txtentydev.setFont(fntdevol);
		
		JLabel lblten = new JLabel("Ten Dollars");
		Utils.txtten = new JTextField(5);
		Utils.txtten.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtten.setText(""+caixa.getTen());
		Utils.txtten.setEditable(false);
		Utils.txtten.setFont(fntcaixa);
		Utils.txttendado = new JFormattedTextField (new Integer (0));
		Utils.txttendado.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txttendado.setEditable(true);
		Utils.txttendado.setFont(fntdado);	
		Utils.txttendev = new JTextField(5);
		Utils.txttendev.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txttendev.setEditable(false);
		Utils.txttendev.setFont(fntdevol);
		
		JLabel lblfive = new JLabel("Five Dollars");
		Utils.txtfive = new JTextField(5);
		Utils.txtfive.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtfive.setText(""+caixa.getFive());
		Utils.txtfive.setEditable(false);
		Utils.txtfive.setFont(fntcaixa);
		Utils.txtfivedado = new JFormattedTextField (new Integer (0));
		Utils.txtfivedado.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtfivedado.setEditable(true);
		Utils.txtfivedado.setFont(fntdado);
		Utils.txtfivedev = new JTextField(5);
		Utils.txtfivedev.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtfivedev.setEditable(false);
		Utils.txtfivedev.setFont(fntdevol);
		
		JLabel lbldollar = new JLabel("Dollar");
		Utils.txtdollar = new JTextField(5);
		Utils.txtdollar.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtdollar.setText(""+caixa.getOne());
		Utils.txtdollar.setEditable(false);
		Utils.txtdollar.setFont(fntcaixa);
		Utils.txtdollardado = new JFormattedTextField (new Integer (0));
		Utils.txtdollardado.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtdollardado.setEditable(true);
		Utils.txtdollardado.setFont(fntdado); 
		Utils.txtdollardev = new JTextField(5);
		Utils.txtdollardev.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtdollardev.setEditable(false);
		Utils.txtdollardev.setFont(fntdevol); 
		
		JLabel lblquarter = new JLabel("Quarter");
		Utils.txtquarter = new JTextField(5);
		Utils.txtquarter.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtquarter.setText(""+caixa.getQuarter());
		Utils.txtquarter.setEditable(false);
		Utils.txtquarter.setFont(fntcaixa);
		Utils.txtquarterdado = new JFormattedTextField (new Integer (0));
		Utils.txtquarterdado.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtquarterdado.setEditable(true);
		Utils.txtquarterdado.setFont(fntdado);
		Utils.txtquarterdev = new JTextField(5);
		Utils.txtquarterdev.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtquarterdev.setEditable(false);
		Utils.txtquarterdev.setFont(fntdevol);
		
		JLabel lbldime = new JLabel("Dime");
		Utils.txtdime = new JTextField(5);
		Utils.txtdime.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtdime.setText(""+caixa.getDime());
		Utils.txtdime.setEditable(false);
		Utils.txtdime.setFont(fntcaixa);
		Utils.txtdimedado = new JFormattedTextField (new Integer (0));
		Utils.txtdimedado.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtdimedado.setEditable(true);
		Utils.txtdimedado.setFont(fntdevol);
		Utils.txtdimedev = new JTextField(5);
		Utils.txtdimedev.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtdimedev.setEditable(false);
		Utils.txtdimedev.setFont(fntdevol);
		
		JLabel lblnickel = new JLabel("Nickel");
		Utils.txtnickel = new JTextField(5);
		Utils.txtnickel.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtnickel.setText(""+caixa.getNickel());
		Utils.txtnickel.setEditable(false);
		Utils.txtnickel.setFont(fntcaixa);
		Utils.txtnickeldado = new JFormattedTextField (new Integer (0));
		Utils.txtnickeldado.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtnickeldado.setEditable(true);
		Utils.txtnickeldado.setFont(fntdado);
		Utils.txtnickeldev = new JTextField(5);
		Utils.txtnickeldev.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtnickeldev.setEditable(false);
		Utils.txtnickeldev.setFont(fntdevol);
		
		JLabel lblpenny = new JLabel("Penny");
		Utils.txtpenny = new JTextField(5);
		Utils.txtpenny.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtpenny.setText(""+caixa.getPenny());
		Utils.txtpenny.setEditable(false);
		Utils.txtpenny.setFont(fntcaixa);
		Utils.txtpennydado = new JFormattedTextField (new Integer (0));
		Utils.txtpennydado.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtpennydado.setEditable(true);
		Utils.txtpennydado.setFont(fntdado);
		Utils.txtpennydev = new JTextField(5);
		Utils.txtpennydev.setHorizontalAlignment(JTextField.RIGHT);
		Utils.txtpennydev.setEditable(false);
		Utils.txtpennydev.setFont(fntdevol);
		
		
		Utils.jtxtftotalmoney= new JTextField();
		Utils.jtxtftotalmoney.setHorizontalAlignment(JTextField.RIGHT);
		Utils.jtxtftotalmoney.setEditable(false);
		Utils.jtxtftotalmoney.setText(""+caixa.getTotalmoney());
		
		Utils.sumadado = new JTextField();
		Utils.sumadado.setHorizontalAlignment(JTextField.RIGHT);
		Utils.sumadado.setEditable(false);
		
		Utils.sumadevuelto = new JTextField();
		Utils.sumadevuelto.setHorizontalAlignment(JTextField.RIGHT);
		Utils.sumadevuelto.setEditable(false);
				
		JButton btncorrecto = new JButton("OK");
		btncorrecto.addActionListener(new checkCashRegister());
				
		caja.add(new JLabel("KIND COIN"));
		caja.add(new JLabel("DISPONIBLE"));
		caja.add(new JLabel("ENTREGADO"));
		caja.add(new JLabel("DEVOLUCI�N"));
		caja.add(lblonehundred);
		caja.add(Utils.txtonehundred);
		caja.add(Utils.txtonehundreddado);
		caja.add(Utils.txtonehundreddev);
		caja.add(lbltwenty);
		caja.add(Utils.txtenty);
		caja.add(Utils.txtentydado);
		caja.add(Utils.txtentydev);
		caja.add(lblten);
		caja.add(Utils.txtten);
		caja.add(Utils.txttendado);
		caja.add(Utils.txttendev);
		caja.add(lblfive);
		caja.add(Utils.txtfive);
		caja.add(Utils.txtfivedado);
		caja.add(Utils.txtfivedev);
		caja.add(lbldollar);
		caja.add(Utils.txtdollar);
		caja.add(Utils.txtdollardado);
		caja.add(Utils.txtdollardev);
		caja.add(lblquarter);
		caja.add(Utils.txtquarter);
		caja.add(Utils.txtquarterdado);
		caja.add(Utils.txtquarterdev);
		caja.add(lbldime);
		caja.add(Utils.txtdime);
		caja.add(Utils.txtdimedado);
		caja.add(Utils.txtdimedev);
		caja.add(lblnickel);
		caja.add(Utils.txtnickel);
		caja.add(Utils.txtnickeldado);
		caja.add(Utils.txtnickeldev);
		caja.add(lblpenny);
		caja.add(Utils.txtpenny);
		caja.add(Utils.txtpennydado);
		caja.add(Utils.txtpennydev);
		caja.add(new JLabel(""));
		caja.add(Utils.jtxtftotalmoney);
		caja.add(Utils.sumadado);
		caja.add(Utils.sumadevuelto);
		caja.add(btncorrecto);
	
		panelbtn.setVisible(true);

		panel.add(ventana);
		panel.add(panelbtn);
		panel.add(caja);
		f.setLocationRelativeTo(null);
		f.setVisible(true);
	
	}

}
