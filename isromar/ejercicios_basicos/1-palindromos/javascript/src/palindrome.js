export function palindrome(aString) {
    // Good luck!

    //Copio la variable para no modificarla
    var newaString = aString;

    //Pone los caracteres en minúsculas
    newaString = newaString.toLowerCase();

    //Elimina lo que no sean letras
    newaString = newaString.replace(/[^a-z]/g, '');

    //Separa la cadena de texto en array con items de un carácter
    newaString = newaString.split('');

    //Da la longitud del array
    var len = newaString.length;

    //Muestra la cabecera del resultado
    console.log("\n\n--------- Frase a comprobar: "+aString+" ---------");

    /***Compara cuando un carácter no coindice. Había usado la función reverse() pero si inicio
    la comprobación del primer carácter con el último, no hace falta invertir.***/
    for (var i = 0;i<=len/2;i++){
        console.log("Compara aString["+i+"]=" + newaString[i]+ " con aString["+(len-i-1)+"]=" + newaString[len-i-1]);

        //Si encuentra un carácter que no es igual devuelve false
        if (newaString[i] !== newaString[len-i-1]){
            console.log("---------------- ¡¡¡ No es palíndromo !!! ----------------");
            return false;
        }
    }
    // Mensaje final
    console.log("---------------- ### Sí es palíndromo ### ----------------");
    return true;
}
