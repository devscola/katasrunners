export class Greeting {
    greet(name) {

        var name = name.toString()
        
        if (name === null) {
            return "Hello, my friend"
        } else if (name === name.toUpperCase()) {
            return "HELLO " + name.toUpperCase() + "!"
        } else if (name === [name, name]){
            return "Hello, " + name.join("and") 
        }else if (name === [name, name, name]){
            return "Hello, " + name.join(",","and")
        }else if (name === [name, name, name.toUpperCase()]){
            return "Hello, " + name.join("and") + "." + "AND HELLO" + name.toUpperCase() + "!"
        } else {
            return "Hello, "+ name
        }
    }
   }
  
  

