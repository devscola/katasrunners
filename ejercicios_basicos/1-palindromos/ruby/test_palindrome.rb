require_relative 'palindrome'
require 'test/unit'

class TestPalindrome < Test::Unit::TestCase
  def test_a_palindrome_should_return_boolean
    some_text = 'hello'

    result = palindrome(some_text)

    assert_boolean(result)
  end

  def test_b_should_return_true_when_given_a_palindrome
    some_palindrome = 'eye'

    result = palindrome(some_palindrome)

    assert_true(result)
  end

  def test_c_should_return_false_when_not_given_a_palindrome
    not_palindrome = 'not a palindrome'

    result = palindrome(not_palindrome)

    assert_false(result)
  end

  def test_d_should_ignore_word_casing
    capitalized_palindrome = 'Eye'

    result = palindrome(capitalized_palindrome)

    assert_true(result)
  end

  def test_e_should_ignore_non_alphanumeric_characters
    text_with_a_palindrome = 'My age is 0, 0 si ega ym.'

    result = palindrome(text_with_a_palindrome)

    assert_true(result)
  end
end
