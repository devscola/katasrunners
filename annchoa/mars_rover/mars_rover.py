class North():

    def get_left(self):
        return West()

    def get_right(self):
        return East()

    def get_displacement_vector(self):
        return (1, 0)

class South():

    def get_left(self):
        return East()

    def get_right(self):
        return West()

    def get_displacement_vector(self):
        return (-1, 0)

class East():

    def get_left(self):
        return North()

    def get_right(self):
        return South()

    def get_displacement_vector(self):
        return (0, 1)

class West():

    def get_left(self):
        return South()

    def get_right(self):
        return North()

    def get_displacement_vector(self):
        return (0, -1)

class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def is_equal(self, point):
        return self.x == point.x and self.y == point.y

    def displace(self, vector):
        self.x += vector[0]
        self.y += vector[1]

class MarsRover():

    def __init__(self):

        self.where_is_facing = North()
        self.position = Point(0, 0)

    def are_you_in_position(self, x, y):
        return self.position.is_equal(Point(x, y))

    def move(self):
        direction_vector = self.where_is_facing.get_displacement_vector()
        self.position.displace(direction_vector)

    def turn_left(self):
        self.where_is_facing = self.where_is_facing.get_left()

    def turn_right(self):
        self.where_is_facing = self.where_is_facing.get_right()
