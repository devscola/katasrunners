const romanNumerals = [
    {'I': 1}, 
    {'V': 5}, 
    {'X': 10}, 
    {'L': 50}, 
    {'C': 100}, 
    {'D': 500},
    {'M': 1000}
];

export function convertToRoman(num) {

    const romanNum = replaceWithRomanNumerals(num);

    const correctedNum = correctRomanNum(romanNum);

    return correctedNum;
}

function replaceWithRomanNumerals(num) {
    let romanNum = '';

    for (let i = romanNumerals.length - 1; i >= 0; i--) {
        while (num >= Object.values(romanNumerals[i])) {
            romanNum += Object.keys(romanNumerals[i]);
            num -= Object.values(romanNumerals[i]);
        }            
    }
    
    return romanNum;
}

function correctRomanNum(romanNum) {
    const correctNum = romanNum
    .replace("VIIII", "IX")
    .replace("IIII", "IV")
    .replace("LXXXX", "XC")
    .replace("XXXX", "XL")
    .replace("DCCCC", "CM")
    .replace("CCCC", "CD");

    return correctNum;
}