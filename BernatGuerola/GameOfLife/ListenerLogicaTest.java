package principal;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;

import org.junit.jupiter.api.Test;

class ListenerLogicaTest {
	
	ListenerLogica testear = new ListenerLogica();

	@Test
	void liveAnd1Neighbour() {
		//1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
		int contVecinos = 1;
		Color color = Color.GREEN;
		Color expected=Color.LIGHT_GRAY;	
		Color result= testear.colordonat(contVecinos, color);
		
		assertEquals(expected,result);	
	}
	
	@Test
	void liveAnd0Neighbour() {
		//1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
		int contVecinos = 0;
		Color color = Color.GREEN;
		Color expected=Color.LIGHT_GRAY;	
		Color result= testear.colordonat(contVecinos, color);
		
		assertEquals(expected,result);	
	}
	
	@Test
	void liveAnd4Neighbour() {
		//2. Any live cell with more than three live neighbours dies, as if by overcrowding.
		int contVecinos = 4;
		Color color = Color.GREEN;
		Color expected=Color.LIGHT_GRAY;	
		Color result= testear.colordonat(contVecinos, color);
		
		assertEquals(expected,result);
		
	}
	
	@Test
	void liveAnd5Neighbour() {
		//2. Any live cell with more than three live neighbours dies, as if by overcrowding.
		int contVecinos = 5;
		Color color = Color.GREEN;
		Color expected=Color.LIGHT_GRAY;	
		Color result= testear.colordonat(contVecinos, color);
		
		assertEquals(expected,result);
		
	}
	@Test
	void liveAnd6Neighbour() {
		//2. Any live cell with more than three live neighbours dies, as if by overcrowding.
		int contVecinos = 6;
		Color color = Color.GREEN;
		Color expected=Color.LIGHT_GRAY;	
		Color result= testear.colordonat(contVecinos, color);
		
		assertEquals(expected,result);
		
	}
	@Test
	void liveAnd7Neighbour() {
		//2. Any live cell with more than three live neighbours dies, as if by overcrowding.
		int contVecinos = 7;
		Color color = Color.GREEN;
		Color expected=Color.LIGHT_GRAY;	
		Color result= testear.colordonat(contVecinos, color);
		
		assertEquals(expected,result);
		
	}
	@Test
	void liveAnd8Neighbour() {
		//2. Any live cell with more than three live neighbours dies, as if by overcrowding.
		int contVecinos = 8;
		Color color = Color.GREEN;
		Color expected=Color.LIGHT_GRAY;	
		Color result= testear.colordonat(contVecinos, color);
		
		assertEquals(expected,result);
		
	}
	
	@Test
	void liveAnd2Neighbour() {
		//3. Any live cell with two or three live neighbours lives on to the next generation.
		int contVecinos = 2;
		Color color = Color.GREEN;
		Color expected=Color.GREEN;	
		Color result= testear.colordonat(contVecinos, color);
		
		assertEquals(expected,result);	
	}
	
	@Test
	void liveAnd3Neighbour() {
		//3. Any live cell with two or three live neighbours lives on to the next generation.
		int contVecinos = 3;
		Color color = Color.GREEN;
		Color expected=Color.GREEN;	
		Color result= testear.colordonat(contVecinos, color);
		
		assertEquals(expected,result);	
	}
	
	@Test
	void deadAnd3Neighbour() {
		//4. Any dead cell with exactly three live neighbours becomes a live cell.
		int contVecinos = 3;
		Color color = Color.LIGHT_GRAY;
		Color expected=Color.GREEN;	
		Color result= testear.colordonat(contVecinos, color);
		
		assertEquals(expected,result);	
	}

}
