class TennisScore():
    def __init__(self):
        self.player1 = 0
        self.player2 = 0

    def points(self):
        game = GameScore(self.player1, self.player2)
        return game.points()

    def getGameScore(self):
        game = GameScore(self.player1, self.player2)
        return game.getScore()

    def getSetScore(self):
        game = GameSetScore(self.player1, self.player2)
        return game.getSetScore()

    def getMatch(self):
        game = GetMatch(self.player1, self.player2)
        return game.getMatch()

class GameScore():
    def __init__(self, player1, player2):
        self.player1 = player1
        self.player2 = player2

    def points(self):
        return self.player1, self.player2

    def getScore(self):

        if self.isDeuce():
            return 'Deuce'

        if self.isWinner(self.player1, self.player2):
            return 'Winner: player1'

        if self.isWinner(self.player2, self.player1):
            return 'Winner: player2'

        if self.isAdvantage(self.player1, self.player2):
            return 'Advantage: player1'

        if self.isAdvantage(self.player2, self.player1):
            return 'Advantage: player2'

        return self.getScoreName(self.player1, self.player2)

    def isWinner(self, aPlayer, anotherPlayer):
        return aPlayer > anotherPlayer and aPlayer >= 4 and (aPlayer - anotherPlayer) >=2

    def isAdvantage(self, aPlayer, anotherPlayer):
        return aPlayer == 4 and anotherPlayer == 3

    def getScoreName(self, point_player1, point_player2):
        points_name = [
            'love',
            'fifteen',
            'thirty',
            'forty'
                    ]
        return points_name[point_player1], points_name[point_player2]

    def isDeuce(self):
        return self.player1 == self.player2 and self.player1 >= 3

class GameSetScore():
    def __init__(self, player1, player2):
        self.player1 = player1
        self.player2 = player2

    def getSetScore(self):

        if self.anotherWinner(self.player1, self.player2):
            return 'Player 1 winner'

        if self.anotherWinner(self.player2, self.player1):
            return 'Player 2 winner'

        if self.isWinner(self.player1, self.player2):
            return 'Player 1 won a set'

        if self.isWinner(self.player2, self.player1):
            return 'Player 2 won a set'

        if self.additionalGame(self.player1, self.player2):
            return 'Additional game'

        if self.additionalGame(self.player2, self.player1):
            return 'Additional game'

        if self.tieBreak(self.player1, self.player2):
            return 'Tie-break'

        if self.tieBreakButOneMorePoint(self.player1, self.player2):
            return 'Player 1 winner'

        if self.tieBreakButOneMorePoint(self.player2, self.player1):
            return 'Player 2 winner'

    def isWinner(self, aPlayer, anotherPlayer):
        return aPlayer > anotherPlayer and aPlayer >= 6 and (aPlayer - anotherPlayer) >= 2

    def anotherWinner(self, aPlayer, anotherPlayer):
        return aPlayer == 7 and anotherPlayer == 5

    def additionalGame(self, aPlayer, anotherPlayer):
        return aPlayer == 6 and anotherPlayer == 5

    def tieBreak(self, aPlayer, anotherPlayer):
        return aPlayer == anotherPlayer and aPlayer == 6

    def tieBreakButOneMorePoint(self, aPlayer, anotherPlayer):
        return aPlayer == 7 and anotherPlayer == 6

class GetMatch(self):
    def __init__(self, player1, player2):
        self.player1 = player1
        self.player2 = player2

    def getMatch(self):
        if self.isMatch(self.player1, self.player2):
            return 'Player 1 win the match'

    def isMatch(self, aPlayer, anotherPlayer):
        return aPlayer == 2 and anotherPlayer == 1
