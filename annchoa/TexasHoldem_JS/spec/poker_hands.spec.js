import { PokerHands } from '../src/poker_hands.js'

describe('Poker Hands', () => {

    it('should tell me who has the higher card', () => {
      // Arrange
      const firstHand = ["2H", "3D", "5S", "9C", "KD"]
      const secondHand = ["2C", "3H", "4S", "8C", "AH"]
      const poker_hands = new PokerHands(firstHand, secondHand)
      const expected_value = "The winner is secondHand"
      // Act
      const winner = poker_hands.calculateWinner()
      // Assert
      expect(winner).toBe(expected_value)
    })

})
