package principal;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class Disseny {
	
	private static boolean defaultcolor = true;
	public static JLabel etiqueta;
	
	public class ActionListenerNou implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent nou) {
			Utils.intents=1;
			Disseny.etiqueta.setText("Generation n�: " + Utils.intents + " ");
			for (int i = 0; i<Utils.DIMENSION;i++) {
				for (int j =0; j<Utils.DIMENSION;j++) {
					changingRandomColor(Utils.matriu[i][j]);
				}
			}
		}
	}
	
	public static void randomColors() {
		Utils.creaBotones();
		
		for (int i = 0; i<Utils.DIMENSION;i++) {
			for (int j =0; j<Utils.DIMENSION;j++) {
				changingRandomColor(Utils.matriu[i][j]);
			}
		}
	}
	
	public static void changingRandomColor(JButton br) {
		//br = new JButton();
		int numAlea = (int) Math.floor(Math.random() * 2 + 1);
		if (numAlea == 2) {
			br.setBackground(Color.GREEN);
		} else {
			br.setBackground(Color.LIGHT_GRAY);
		}	
	}
	
	public class ActionListenerTest implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object source = e.getSource();
			
			for (int i = 0; i<Utils.DIMENSION;i++) {
				for (int j =0; j<Utils.DIMENSION;j++) {
					if (source == Utils.matriu[i][j]) {
						changingColor(Utils.matriu[i][j]);
					}
				}
			}
		}
	}

	private void changingColor(JButton b) {
		if (defaultcolor) {
			defaultcolor = false;
			b.setBackground(Color.GREEN);
		} else {
			defaultcolor = true;
			b.setBackground(Color.LIGHT_GRAY);
		}
	}

	public void board() {
		//creaBotones();
		randomColors();
		for (int i = 0; i<Utils.DIMENSION;i++) {
			for (int j =0; j<Utils.DIMENSION;j++) {
				Utils.matriu[i][j].addActionListener(new ActionListenerTest());
			}
		}

		JFrame f = new JFrame();
		f.setTitle("Game of Life");
		f.setSize(365, 320);
		f.setResizable(false);
		
		JPanel panelbtn = new JPanel();
		JPanel options = new JPanel();
		f.setLayout(new FlowLayout());
		
		JButton b = new JButton("Next generation");
		b.addActionListener(new ListenerLogica());
		etiqueta = new JLabel();
		JLabel relleno = new JLabel();
		relleno.setText("   ");
		etiqueta.setText(Utils.missatge);
		etiqueta.setFont(new Font("Tahoma", Font.BOLD,18));
		etiqueta.setForeground(Color.BLUE);
		
		options.add(b);
		options.add(relleno);
		options.add(etiqueta);
		
		panelbtn.setLayout(new GridLayout(Utils.DIMENSION, Utils.DIMENSION));
		
		JMenuBar menubar = new JMenuBar();// barra men�
		JMenu menu = new JMenu("Men�");// opci�n men�
		menubar.add(menu);
		JMenuItem reiniciar = new JMenuItem("Start again");
		JMenuItem salir = new JMenuItem("Exit");
		menu.add(reiniciar);
		menu.add(salir);

		f.setJMenuBar(menubar);

		reiniciar.addActionListener(new ActionListenerNou());

		salir.addActionListener(new ActionListener(){//opci�n salir

			@Override
			public void actionPerformed(ActionEvent arg0) {
				f.dispose();
			}
				
			});

		for (int i = 0; i<Utils.DIMENSION;i++) {
			for (int j =0; j<Utils.DIMENSION;j++) {
				panelbtn.add(Utils.matriu[i][j]);
			}
		}
		
		panelbtn.setVisible(true);

		f.add(panelbtn);
		f.add(options);
		f.setLocationRelativeTo(null);
		f.setVisible(true);
	}
}
