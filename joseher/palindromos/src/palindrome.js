export function palindrome(aString) {
    let string_united = stringModifier(aString)
    let string_reversed = string_united.split("").reverse().join("")
    
    return string_united == string_reversed;
}

function stringModifier(theString){
    return theString.replace(/[\W_]/g, "").toLowerCase()
}