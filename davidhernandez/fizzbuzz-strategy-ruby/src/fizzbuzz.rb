class Rule
  def initialize(number)
    @number = number
  end

  def applies
    return true
  end

  def to_s
    @number.to_s
  end
end

class FizzRule < Rule
  def applies
    return @number % 3 == 0
  end

  def to_s
    return 'Fizz'
  end
end

class BuzzRule < Rule
  def applies
    return @number % 5 == 0
  end

  def to_s
    return 'Buzz'
  end
end

class FizzBuzzRule < Rule
  def applies
    return @number % 5 == 0 && @number % 3 == 0
  end

  def to_s
    return 'FizzBuzz'
  end
end

class FizzBuzz
  def initialize(number)
    @number = number
  end

  def convert
    rules = [
      FizzBuzzRule.new(@number),
      FizzRule.new(@number),
      BuzzRule.new(@number),
      Rule.new(@number)
    ]

    rules.each { |rule| return rule.to_s if rule.applies }
  end
end
