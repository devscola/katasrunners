const BASE_PRICE = 8
const DISCOUNT_FOR_TWO_BOOKS = 0.95
const DISCOUNT_FOR_THREE_BOOKS = 0.90
export class PotterBooks {
    
    calculatePrice(books){
        const copies = Object.values(books)
        const quantityOfBooks = this.getQuantityOfBook(copies)

        let  totalPrice =  BASE_PRICE * quantityOfBooks 
        if(this.applyDiscountForTwoBooks(copies)){
            totalPrice *= DISCOUNT_FOR_TWO_BOOKS
        }
        if(this.applyDiscountForThreeBooks(copies)){
            totalPrice *= DISCOUNT_FOR_THREE_BOOKS
        }

        return totalPrice
    }

    applyDiscountForTwoBooks(copies){
        return copies.length === 2
    }
    applyDiscountForThreeBooks(copies){
        return copies.length === 3
    }

    getQuantityOfBook(copies){
        return copies.reduce((totalQuantity,quantityOfCopies) => {
            return  totalQuantity + quantityOfCopies
        })
    }
}
