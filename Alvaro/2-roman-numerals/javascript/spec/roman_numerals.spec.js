import { convertToRoman } from '../src/roman_numerals'

describe('The Roman numerals conversion', () => {
    it ('Converts a single number', () => {
        // Arrange
        const expected_value = 'I'
        const original_number = 1

        // Act
        const result = convertToRoman(original_number)

        // Assert
        expect(result).toBe(expected_value)
    })

    it ('Uses the same symbol multiple times', () => {
        // Arrange
        const expected_value = 'III'
        const original_number = 3

        // Act
        const result = convertToRoman(original_number)

        // Assert
        expect(result).toBe(expected_value)
    })

    it ('Combines two symbols to convert numbers smaller than nine', () => {
        // Arrange
        const expected_value = 'VIII'
        const original_number = 8

        // Act
        const result = convertToRoman(original_number)

        // Assert
        expect(result).toBe(expected_value)
    })

    it ('Combines three symbols to convert numbers smaller than forty', () => {
        // Arrange
        const expected_value = 'XXXVIII'
        const original_number = 38

        // Act
        const result = convertToRoman(original_number)

        // Assert
        expect(result).toBe(expected_value)
    })

    it ('Combines all symbols to convert numbers', () => {
        // Arrange
        const expected_value = 'MMMCCLVI'
        const original_number = 3256

        // Act
        const result = convertToRoman(original_number)

        // Assert
        expect(result).toBe(expected_value)
    })

    it ('Ensures a symbol is not repeated more than 3 times', () => {
        // Arrange
        const expected_value = 'IV'
        const original_number = 4

        // Act
        const result = convertToRoman(original_number)

        // Assert
        expect(result).toBe(expected_value)
    })
})
