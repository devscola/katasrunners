# Pistas

## Pista 1

Usa `String#ord` para convertir los caracteres en inglés a ASCII. [Link](https://ruby-doc.org/core-2.7.2/String.html#method-i-ord)

## Pista 2

Usa `String#chr` para convertir el ASCII a caracteres en inglés. [Link](https://ruby-doc.org/core-2.7.2/String.html#method-i-chr)

## Pista 3

Deja cualquier cosa que no este entre A-Z sin tocar.
