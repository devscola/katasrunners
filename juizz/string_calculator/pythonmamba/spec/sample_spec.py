from mamba import *
from expects import *
from string_calculator import *

with description('String calculator') as self:
  with it('returns 0 when receive an empty string'):

    result = string_calculator('')

    expect(result).to(equal(0))

  with it('returns a int when receive a string'):

    result = string_calculator('1')

    expect(result).to(equal(1))

  with it('returns an add when receive a two numbers'):

    result = string_calculator('1,2')

    expect(result).to(equal(3))

  with it('returns the total of any amount of numbers'):

    result = string_calculator('1,2,3')

    expect(result).to(equal(6))

  with it('allows \n as number separator'):

    result = string_calculator('1\n2,3')

    expect(result).to(equal(6))

  with it('allows any delimiter'):

    result = string_calculator('//;\n1;2')

    expect(result).to(equal(3))

  with it('exception when negative number'):
    expect(lambda: string_calculator('1, 2, -7')).to(raise_error(TypeError, 'negatives are not allowed'))
