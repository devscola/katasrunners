export function string_calc(stringOfNumbers, splitter) {
    var numberForEmptyString = 0
    if(stringOfNumbers === '') return numberForEmptyString
    const stringSplitter = splitter || ','
    const negatives = detectNegativeNumbers(stringOfNumbers, stringSplitter)
    if(negatives.amount>0) return negatives.list
    else {
        var suma = addString(stringOfNumbers, stringSplitter)
        return suma;
    }    
}

function addString(stringOfNumbers, splitter) {
    var splittedNumbers = stringOfNumbers.split(splitter)
    var total = 0
    splittedNumbers.forEach(number => {
    if(isNaN(parseInt(number)) || (parseInt(number))>1000 ) return
    else total += parseInt(number)
    });
    return total
}

function detectNegativeNumbers(stringOfNumbers, splitter) {
    var splittedNumbers = stringOfNumbers.split(splitter)
    var returnedNegatives = {
        amount  : 0,
        list    : "negatives not allowed:"
    } 
    splittedNumbers.forEach(number => {
        if(number<0) {
            returnedNegatives.amount++
            returnedNegatives.list += " " + number 
        }
    });
    return returnedNegatives
}
