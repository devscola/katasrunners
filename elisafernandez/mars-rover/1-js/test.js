const assert = require('assert');
//const { it } = require('mocha');
const MarsRover = require('./marsRover.js');


describe('Mars Rover', () => {

    it('Knows its landing position', () => {
        const initialPosition = {x: 5, y: 5, d: 'N'};
        const marsRover = new MarsRover(initialPosition);

        assert.equal(JSON.stringify(marsRover.position), JSON.stringify(initialPosition));
    })

    it('Moves forward when facing North', () => {
        const initialPosition = {x: 5, y: 5, d: 'N'};
        const marsRover = new MarsRover(initialPosition);

        marsRover.executeCommand('m');

        const newPosition = {x: 5, y: 6, d: 'N'};
        assert.equal(JSON.stringify(marsRover.position), JSON.stringify(newPosition));

    })

    it('Moves forward several times when facing North', () => {
        const initialPosition = {x: 5, y: 5, d: 'N'};
        const marsRover = new MarsRover(initialPosition);

        marsRover.executeCommand('mm');

        const newPosition = {x: 5, y: 7, d: 'N'};
        assert.equal(JSON.stringify(marsRover.position), JSON.stringify(newPosition));

    })

    it('Moves forward when facing West', () => {
        const initialPosition = {x: 5, y: 5, d: 'W'};
        const marsRover = new MarsRover(initialPosition);

        marsRover.executeCommand('m');

        const newPosition = {x: 4, y: 5, d: 'W'};
        assert.equal(JSON.stringify(marsRover.position), JSON.stringify(newPosition));

    })

    it('Points West when ordered to turn left when facing North', () => {
        const initialPosition = {x: 5, y: 5, d: 'N'};
        const marsRover = new MarsRover(initialPosition);

        marsRover.executeCommand('l');

        const newPosition = {x: 5, y: 5, d: 'W'};
        assert.equal(JSON.stringify(marsRover.position), JSON.stringify(newPosition));
    })

    it('Points North when ordered to turn right twice when facing South', () => {
        const initialPosition = {x: 5, y: 5, d: 'S'};
        const marsRover = new MarsRover(initialPosition);

        marsRover.executeCommand('rr');

        const newPosition = {x: 5, y: 5, d: 'N'};
        assert.equal(JSON.stringify(marsRover.position), JSON.stringify(newPosition));
    })

    it('Executes complex orders correctly', () => {
        const initialPosition = {x: 12, y: 31, d: 'S'};
        const marsRover = new MarsRover(initialPosition);

        marsRover.executeCommand('rlmmmrmmlmml');

        const newPosition = {x: 10, y: 26, d: 'E'};
        assert.equal(JSON.stringify(marsRover.position), JSON.stringify(newPosition));
    })

    it('Goes over border in the positive axis', () => {
        const initialPosition = {x: 1, y: 1, d: 'N'};
        const marsRover = new MarsRover(initialPosition);
        marsRover.worldDimensions = {x: 5, y: 5};
        
        marsRover.executeCommand('mmmmm')

        const newPosition = {x: 1, y: 1, d: 'N'};
        assert.equal(JSON.stringify(marsRover.position), JSON.stringify(newPosition));
    })

    it('Goes over border in the negative axis', () => {
        const initialPosition = {x: 1, y: 1, d: 'S'};
        const marsRover = new MarsRover(initialPosition);
        marsRover.worldDimensions = {x: 5, y: 5};
        
        marsRover.executeCommand('m')

        const newPosition = {x: 1, y: 5, d: 'S'};
        assert.equal(JSON.stringify(marsRover.position), JSON.stringify(newPosition));
    })


})