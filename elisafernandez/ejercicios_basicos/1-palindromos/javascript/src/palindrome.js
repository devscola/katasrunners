export function palindrome(aString) {
  const normalizedString = normalizeString(aString);
  const reversedString = reverseString(normalizedString);

  return normalizedString === reversedString;
}

function normalizeString(string) {
  const uppercaseString = string.toUpperCase();
  const alphanumericRegex = /[A-Z0-9]/g;
  const alphaNumericString = uppercaseString.match(alphanumericRegex).join('');
  return alphaNumericString;
}

function reverseString(string) {
  return string.split('').reverse().join('');
}
