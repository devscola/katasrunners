import { PotterBooks } from '../src/potterBooks.js'

describe('potterBooks discounts', () => {
    it("should return the book's price", () => {
        // Arrange
        const expected_value = 8
        const potterBooks = new PotterBooks(['Harry Potter y la piedra filosofal'])
        // Act
        const result = potterBooks.price()

        // Assert
        expect(result).toEqual(expected_value)
    })
    it("should return -5% off the price if you buy two different books", () => {
        // Arrange
        const expected_value = 15.20
        const potterBooks = new PotterBooks(['Harry Potter y la piedra filosofal','Orden del Fénix'])
        // Act
        const result = potterBooks.price()

        // Assert
        expect(result).toEqual(expected_value)
    })
    it("should return -10% off the price if you buy three different books", () => {
        // Arrange
        const expected_value = 21.60
        const potterBooks = new PotterBooks(['Harry Potter y la piedra filosofal','Orden del Fénix','El Calix de Fuego'])
        // Act
        const result = potterBooks.price()

        // Assert
        expect(result).toEqual(expected_value)
    })
    it("should return no discount if we buy two equal books", () => {
        // Arrange
        const expected_value = 16
        const potterBooks = new PotterBooks(['Harry Potter y la piedra filosofal','Harry Potter y la piedra filosofal'])
        // Act
        const result = potterBooks.price()

        // Assert
        expect(result).toEqual(expected_value)
    })

    fit("should return 5% of discount just for similar books, if is the same, keep in the current price", () => {
        // Arrange
        const expected_value = 23.20
        const potterBooks = new PotterBooks(['Harry Potter y la piedra filosofal','Harry Potter y la piedra filosofal','Orden del Fénix'])
        // Act
        const result = potterBooks.price()

        // Assert
        expect(result).toEqual(expected_value)
    })
})
