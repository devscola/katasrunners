import { example, palindromo } from '../src/palindromo.js'

describe('Palindromo', () => {
    it('should return true', () => {
        // Arrange
        const expected_value = true

        // Act
        const result = palindromo('eye')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return true', () => {
        // Arrange
        const expected_value = true

        // Act
        const result = palindromo('_eye')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return true', () => {
        // Arrange
        const expected_value = true

        // Act
        const result = palindromo('race car')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return false', () => {
        // Arrange
        const expected_value = false

        // Act
        const result = palindromo('not a palindrome')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return true', () => {
        // Arrange
        const expected_value = true

        // Act
        const result = palindromo('A man, a plan, a canal. Panama')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return true', () => {
        // Arrange
        const expected_value = true

        // Act
        const result = palindromo('never odd or even')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return false', () => {
        // Arrange
        const expected_value = false

        // Act
        const result = palindromo('nope')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return false', () => {
        // Arrange
        const expected_value = false

        // Act
        const result = palindromo('almostomla')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return true', () => {
        // Arrange
        const expected_value = true

        // Act
        const result = palindromo('My age is 0, 0 si ega ym.')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return false', () => {
        // Arrange
        const expected_value = false

        // Act
        const result = palindromo('1 eye for of 1 eye.')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return false', () => {
        // Arrange
        const expected_value = false

        // Act
        const result = palindromo('1 eye for of 1 eye.')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return true', () => {
        // Arrange
        const expected_value = true

        // Act
        const result = palindromo('0_0 (: /-\ :) 0-0')

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return false', () => {
        // Arrange
        const expected_value = false

        // Act
        const result = palindromo('five|\_/|four')

        // Assert
        expect(result).toBe(expected_value)
    })

})
