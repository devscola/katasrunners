const assert = require('assert');
const FizzBuzz = require('./FizzBuzz.js');

describe('FizzBuzz', function() {
    it('should return an array with 100 numbers', function() {
        const fizzLength = FizzBuzz().length;
        assert.equal(fizzLength, 100);
    });

    it('should not return numbers lower than 1', function() {
        const lowerThanOne = FizzBuzz().find(number => number < 1);
        assert.equal(lowerThanOne, undefined);
    })

    it('should not return numbers higher than 100', function() {
        const lowerThanOne = FizzBuzz().find(number => number > 100);
        assert.equal(lowerThanOne, undefined);
    })

    it('should not return any multiple of 3', function() {
        const multipleOfThree = FizzBuzz().find(number => number % 3 === 0);
        assert.equal(multipleOfThree, undefined);
    })

    it('should return Fizz for multiples of 3', function() {
        const fizz = FizzBuzz()[5];
        assert.equal(fizz, 'Fizz');
    })

    it('should not return any multiple of 5', function() {
        const multipleOfFive = FizzBuzz().find(number => number % 5 === 0);
        assert.equal(multipleOfFive, undefined);
    })

    it('should return Buzz for multiples of 5', function() {
        const buzz = FizzBuzz()[9];
        assert.equal(buzz, 'Buzz');
    })

    it('should return FizzBuzz for multiples of 15', function() {
        const fizzBuzz = FizzBuzz()[14];
        assert.equal(fizzBuzz, 'FizzBuzz');
    })

    it('a number is Fizz if it contains a 3', function() {
        const fizz = FizzBuzz()[12];
        assert.equal(fizz, 'Fizz');
    })
    
    it('a number is Buzz if it contains a 5', function() {
        const buzz = FizzBuzz()[51];
        assert.equal(buzz, 'Buzz');
    })
    
  });